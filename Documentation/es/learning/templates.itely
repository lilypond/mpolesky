@c -*- coding: utf-8; mode: texinfo; documentlanguage: es -*-

@ignore
    Translation of GIT committish: 9cb0f67cda719b296b698a8c25b2559b4f427148

    When revising a translation, copy the HEAD committish of the
    version that you are working on.  For details, see the Contributors'
    Guide, node Updating translation committishes..
@end ignore

@c \version "2.12.0"

@node Plantillas
@appendix Plantillas
@translationof Templates

Esta sección del manual contiene plantillas con la partitura de LilyPond
ya preparada.  Sólo tiene que escribir las notas, lanzar LilyPond y ¡disfrutar de
unas hermosas partituras impresas!

@c bad node name for ancient notation to avoid conflict
@menu
* Pentagrama único::
* Plantillas de piano::
* Cuarteto de cuerda::
* Conjuntos vocales::
* Plantillas orquestales::
* Plantillas para notación antigua::
* Otras plantillas::
@end menu


@node Pentagrama único
@appendixsec Pentagrama único
@translationof Single staff

@appendixsubsec Sólo notas

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc,addversion]
{single-staff-template-with-only-notes.ly}


@appendixsubsec Notas y letra

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc,addversion]
{single-staff-template-with-notes-and-lyrics.ly}

@appendixsubsec Notas y acordes

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{single-staff-template-with-notes-and-chords.ly}

@appendixsubsec Notas, letra y acordes.

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{single-staff-template-with-notes,-lyrics,-and-chords.ly}


@node Plantillas de piano
@appendixsec Plantillas de piano
@translationof Piano templates

@appendixsubsec Piano solo

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{piano-template-simple.ly}

@appendixsubsec Piano y melodía con letra

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{piano-template-with-melody-and-lyrics.ly}

@appendixsubsec Piano con letra centrada

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{piano-template-with-centered-lyrics.ly}

@appendixsubsec Piano con dinámicas centradas

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{piano-template-with-centered-dynamics.ly}


@node Cuarteto de cuerda
@appendixsec Cuarteto de cuerda
@translationof String quartet

@appendixsubsec Cuarteto de cuerda

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{string-quartet-template-simple.ly}

@appendixsubsec Particellas de cuarteto de cuerda

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{string-quartet-template-with-separate-parts.ly}


@node Conjuntos vocales
@appendixsec Conjuntos vocales
@translationof Vocal ensembles

@appendixsubsec Partitura vocal SATB

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{vocal-ensemble-template.ly}

@appendixsubsec Partitura vocal SATB y reducción para piano automática

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{vocal-ensemble-template-with-automatic-piano-reduction.ly}

@appendixsubsec SATB con contextos alineados

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{vocal-ensemble-template-with-lyrics-aligned-below-and-above-the-staves.ly}

@appendixsubsec Estrofa para solista y estribillo a dos voces

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{vocal-ensemble-template-with-verse-and-refrain.ly}


@node Plantillas orquestales
@appendixsec Plantillas orquestales
@translationof Orchestral templates

@appendixsubsec Orquesta, coro y piano
@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{orchestra,-choir-and-piano-template.ly}


@c bad node name to avoid node name conflict
@node Plantillas para notación antigua
@appendixsec Plantillas para notación antigua
@translationof Ancient notation templates

@appendixsubsec Transcripción de música mensural

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{ancient-notation-template----modern-transcription-of-mensural-music.ly}

@appendixsubsec Plantilla para transcripción de canto gregoriano

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{ancient-notation-template----modern-transcription-of-gregorian-music.ly}


@node Otras plantillas
@appendixsec Otras plantillas
@translationof Other templates

@appendixsubsec Combo de jazz
@translationof Jazz combo

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{jazz-combo-template.ly}


