@c -*- coding: utf-8; mode: texinfo; -*-
@ignore
    Translation of GIT committish:  4f7427e7a314a71bde16559247885544256b8213

    When revising a translation, copy the HEAD committish of the
    version that you are working on.  For details, see the Contributors'
    Guide, node Updating translation committishes.
@end ignore

@c Translators: Till Paala

@include included/authors.itexi
@include included/helpus.itexi

@node Gemeinschaft
@unnumbered Gemeinschaft
@translationof Community


@divClass{column-center-top}
@subheading Zusammenarbeit mit der Gemeinschaft

@itemize

@item
@ref{Kontakt}: hier erhalten Sie Hilfe, können diskutieren und
in in Kontakt mit der Gemeinschaft bleiben.

@item
@ref{Minimalbeispiele}: sie werden @emph{sehr stark} empfohlen,
wenn man über LilyPond diskutieren will.

@item
@ref{Fehlerberichte}: wenn etwas schief gelaufen ist.

@end itemize
@divEnd

@divClass{column-left-bottom}
@subheading LilyPond besser machen

@itemize

@item
@ref{Helfen Sie uns}: Ihre Mitarbeit wird benötigt.

@item
@ref{Entwicklung}: für Mitarbeiter und Tester.

@item
@ref{Autoren}: die Menschen, die LilyPond zu dem gemacht haben,
was es jetzt ist.

@end itemize
@divEnd

@divClass{column-right-bottom}
@subheading Verschiedenes

@itemize

@item
@ref{Veröffentlichungen}: was wir geschrieben haben oder was über
uns geschrieben wurde.

@item
@ref{Ältere Neuigkeiten}: ein Archiv.

@end itemize
@divEnd


@divClass{hide}
@menu
* Kontakt::
* Minimalbeispiele::
* Fehlerberichte::
* Helfen Sie uns::
* Entwicklung::
* Autoren::
* Veröffentlichungen::
* Ältere Neuigkeiten::
@end menu
@divEnd


@node Kontakt
@unnumberedsec Kontakt
@translationof Contact


@divClass{column-left-bottom}
@subheading Benutzergruppen und Hilfe

@subsubheading Mailingliste der Benutzer: @code{lilypond-user@@gnu.org}

Diese Mailingliste ist der hauptsächliche Ort, wo Benutzer diskutieren
und einander helfen.  Die Sprache der Liste ist Englisch.  Zu deutschsprachiger Hilfe, siehe unten.

@quotation
@uref{http://mail.gnu.org/mailman/listinfo/lilypond-user,
lilypond-user Abonnement und Info}

@uref{http://mail.gnu.org/archive/html/lilypond-user/,
user Archiv1}
@uref{http://www.mail-archive.com/lilypond-user@@gnu.org/,
Archiv2}
@uref{http://dir.gmane.org/gmane.comp.gnu.lilypond.general,
Archiv3}

@uref{http://post.gmane.org/post.php?group=gmane.comp.gnu.lilypond.general,
mit gmaine an lilypond-user schicken}
@end quotation

@warning{Wenn Sie Fragen stellen (immer auf Englisch) benutzen
Sie bitte @ref{Minimalbeispiele}!}


@subsubheading LilyPond-Schnipsel-Depot

Das LilyPond-Schnipsel-Depot (LSR) ist eine große Sammlung durch die Benutzer
erstellter Beispiele, die frei kopiert und in eigenen Werken benutzt werden
dürfen.  Schauen Sie, was andere Leute konstruiert haben und schreiben Sie
eigene Beispiele!

@example
@uref{http://lsr.dsi.unimi.it}
@end example

Besonders lehrreiche Beispiele aus dem LSR werden in unsere offizielle
Dokumentation eingefügt, in @ref{Schnipsel}.

@subsubheading IRC

Etwas Unterstützung können Sie auch auf unserem IRC-Kanal erhalten:

@example
@uref{irc://irc.freenode.net/lilypond, #lilypond@@irc.freenode.net}
@end example

Dieser Kanal hat kein öffentliches Archiv, sodass jede Frage, die
auch für andere Benutzer nützlich sein könnte, besser auf der
Mailingliste gestellt wird.

@html
<form action="http://webchat.freenode.net"
      method="get"
      name="f_lily_irc"
  <label>irc name:
    <input name="nick" type="text" size="15" value="">
  </label>
  <input name="channels" type="hidden" value="lilypond">
  <input type="submit" value="#lilypond IRC Chat beitreten">
</form>

<script language="JavaScript" type="text/javascript">
  var username = "web";
  var lang = window.navigator.userLanguage ? window.navigator.userLanguage
  : window.navigator.language;
  username += '-' + lang.substr(0, 2);
  username += '-' + navigator.appName.substr(0, 2);
  username += navigator.appCodeName.replace (" ", "").substr(0, 2);
  username += navigator.platform.replace (" ", "").replace("nux", "").replace("n32", "");
  document.forms["f_lily_irc"].nick.value = username;
</script>
@end html

@subsubheading Andere Sprachen

@uref{http://www.lilypondforum.de/,
Deutsches LilyPond-Forum}

@quotation
@uref{http://lists.gnu.org/mailman/listinfo/lilypond-es,
Spanische Mailingliste}

@uref{http://groups.google.com/group/lilypond-brasil,
Portugiesische Gruppe}

@uref{http://lists.gnu.org/mailman/listinfo/lilypond-user-fr,
Französische Mailingliste}

@uref{http://www.lilypondforum.nl/,
Holländisches Forum}
@end quotation

@divEnd


@divClass{column-right-top}
@subheading Bleiben Sie informiert

@subsubheading LilyPond Report

Der einfachste Weg in Kontakt zu bleiben ist es, den Newsletter
unserer Gemeinschaft zu lesen, den LilyPond Report (auf Englisch).

@example
@uref{http://news.lilynet.net}
@end example 

@subsubheading Mailingliste für neue Versionen: @code{info-lilypond@@gnu.org}

Diese Mailingliste ist sehr wenig aktiv und kann nur gelesen werden:
hier wird über neue Versionen von LilyPond informiert.

@quotation
@uref{http://lists.gnu.org/mailman/listinfo/info-lilypond,
info-lilypond Abonnement und Info}

@uref{http://mail.gnu.org/archive/html/info-lilypond/,
info Archiv1}
@uref{http://www.mail-archive.com/info-lilypond@@gnu.org/,
Archiv2}
@uref{http://dir.gmane.org/gmane.comp.gnu.lilypond.announce,
Archiv3}

@c don't include gmane posting here.  -gp
@end quotation


@divEnd


@divClass{column-right-bottom}
@subheading Diskussionen der Entwickler

@subsubheading Entwickler-Mailingliste: @code{lilypond-devel@@gnu.org}

Die meisten Diskussionen von Entwicklern finden auf dieser Liste
statt.  Änderungen sollten hier hin geschickt werden.

@quotation
@uref{http://mail.gnu.org/mailman/listinfo/lilypond-devel,
lilypond-devel Abonnement und Info}

@uref{http://mail.gnu.org/archive/html/lilypond-devel/,
devel Archiv1}
@uref{http://www.mail-archive.com/lilypond-devel@@gnu.org/,
Archiv2}
@uref{http://dir.gmane.org/gmane.comp.gnu.lilypond.devel,
Archiv3}

@uref{http://post.gmane.org/post.php?group=gmane.comp.gnu.lilypond.devel,
mit gmane an lilypond-devel schicken}
@end quotation


@subsubheading Mailingliste für Fehler: @code{bug-lilypond@@gnu.org}

Fehlermeldungen und Diskussionen finden hier statt.

@quotation
@uref{http://mail.gnu.org/mailman/listinfo/bug-lilypond,
bug-lilypond Abonnement und Info}

@uref{http://mail.gnu.org/archive/html/bug-lilypond/,
bug Archiv1}
@uref{http://www.mail-archive.com/bug-lilypond@@gnu.org/,
Archiv2}
@uref{http://dir.gmane.org/gmane.comp.gnu.lilypond.bugs,
Archiv3}

@c don't include gmane posting here.  -gp
@end quotation

@warning{Bevor Sie eine Nachricht an die Fehler-Liste schreiben,
lesen Sie bitte unsere Richtlinien für @ref{Fehlerberichte}.}

@divEnd



@node Minimalbeispiele
@unnumberedsec Minimalbeispiele
@translationof Tiny examples

@divClass{column-center-top}
@subheading Was sind @qq{Minimalbeispiele}?

Ein kleines Beispiel ist ein Beispiel, von dem nichts mehr entfernt
werden kann.
@divEnd

@divClass{column-left-bottom}
@subheading Warum sollte ich so etwas tun?

@divClass{keep-bullets}
@itemize

@item
Je einfacher ein Beispiel ist, um so schneller können mögliche
Hilfeleistende es verstehen und Ihnen helfen.

@item
Ein einfaches Beispiel zeigt, dass Sie sich zuerst Mühe gegeben
haben, das Problem selber zu lösen.  Wenn Leute große Abschnitte
an Code einschicken, sieht es so aus, dass sie sich auch nicht
interessieren, ob ihnen geholfen wird oder nicht.

@item
Ein Minimalbeispiel zu erstellen zwingt Sie dazu zu verstehen,
was vorgeht.  Viele falsche Problemberichte können vermieden werden,
wenn man versucht, erst einmal ein Minimalbeispiel zu erstellen.
Wenn Sie einen @qq{Bug} in Ihrem Minimalbeispiel nicht reproduzieren
können, was das Problem wohl eher zu geringes Verständnis von
LilyPond, nicht jedoch ein Fehler.

@end itemize
@divEnd

@divEnd


@divClass{column-right-bottom}
@subheading Wie soll ich sie erstellen?

@divClass{keep-bullets}
@itemize

@item
Immer die \version -Nummer einfügen.

@item
Machen Sie es klein!  Beispiele zur Platzverteilung oder dem
Seitenlayout können viele Notentakte erfordern, aber die meisten
Probleme können in einem einzigen Takt gezeigt werden.

@item
Wenn Sie versuchen, ein Beispiel zu erstellen, versuchen Sie
zuerst, Abschnitt Ihrer Datei auszukommentieren @w{(@code{%}
oder @code{%@{ @dots{} %@}})}.  Wenn Sie etwas auskommentieren
können, und das Problem immer noch gezeigt wird, entfernen Sie die
auskommentierten Abschnitte.

@item
Vermeiden Sie komplizierte Noten, Tonarten, Schlüssel oder Taktarten, es sei denn Ihr Problem hat mit ihnen etwas zu tun.

@item
Benutzen Sie nicht @code{\override} oder @code{\set}, es sei denn,
der Fehler zeigt sich im Zusammenhang mit diesen Befehlen.

@end itemize
@divEnd

@divEnd




@node Fehlerberichte
@unnumberedsec Fehlerberichte
@translationof Bug reports

@divClass{column-center-top}
@subheading 1. Schritt: Bekannte Fehler

Wenn Sie Eingabecode haben, der einen Programmabsturz oder
fehlerhaften Notensatz erzeugt, dann ist das ein Fehler.  Es gibt
eine Liste der bekannten Fehler beim Google bug tracker:

@example
@uref{http://code.google.com/p/lilypond/issues/list}
@end example

@warning{Bitte fügen Sie @strong{NICHT} selber neue Fehlerberichte
hinzu!  Wenn der Fehler einmal im Bug-Tracker zu sehen ist,
können Sie selber weitere Informationen hinzufügen.}

@divEnd


@divClass{column-left-bottom}
@subheading 2. Schritt: Einen Fehlerbericht erstellen

Wenn Sie einen Fehler entdeckt haben, der nicht aufgelistet ist,
helfen Sie uns bitte, indem Sie einen Fehlerbericht (bug report)
erstellen.

@warning{Wir akzeptieren Fehlerberichte nur als
@ref{Minimalbeispiele}.  Wir haben sehr begrenzte Ressourcen,
um Fehlerberichte zu bearbeiten, weshalb jedes nicht-Minimalbeispiel
zurückgewiesen wird.  Fast jeder Fehler kann mit vier oder sogar
weniger Noten demonstriert werden!}

Hier ein Beispiel eines guten bug reports:

@example
%% the octavation command doesn't
%% change the output at all!

\version "2.10.0"
\relative c''' @{
  c1
  #(set-octavation 1)
  c1
@}
@end example

@divEnd

@divClass{column-right-bottom}
@subheading 3. Schritt:  Einsenden eines Fehlerberichtes

Wenn Sie sichergestellt haben, dass der Fehler noch nicht
bekannt ist und einen Fehlerbericht erstellt haben, senden sie
ihn bitte uns zu!

@divClass{keep-bullets}
@itemize

@item
Wenn Sie die Emailliste @uref{mailto:bug-lilypond@@gnu.org,
+bug-lilypond@@gnu.org} bereits abonniert haben, können Sie
ganz gewöhnlich eine E-Mail schicken.

@item
Wenn Sie nicht Mitglied der Liste sind, können Sie trotzdem
einen Fehlerbericht über die 
@uref{http://post.gmane.org/post.php?group=gmane.comp.gnu.lilypond.bugs,
gmane lilypond.bugs web interface} schicken.

Es gibt jedoch eine genaue Überprüfung auf der Liste, die verhindert,
dass man top-posted.  Diese Überprüfung wird häufig inkorrekt
von LilyPond-Dateien alarmiert.  Darum fügen Sie

@example
> I'm not top posting.
@end example

@noindent
(Sie @emph{müssen} das @code{>} mit einfügen) zu Anfang ihres
Fehlerberichtes hinzu.

@end itemize
@divEnd

Wenn Ihr Fehlerbericht an die Liste versandt wurde, untersucht
unser Fehlerbeseitiger (engl. bug squad) den Bericht.  Warten Sie
bitte 24 Stunden, da uns nur eine geringe Zahl an Helfern zur Verfügung stehen.  Sie werden eventuell nach mehr Information gefragt oder
der Bericht wird zum Tracker hinzugefügt und Sie werden informiert,
welche Nummer er bekommen hat.

Sie können den Fehler so markieren, dass Sie immer eine E-Mail
erhalten, wenn Aktivität für diesen Fehler vorkommt.  Hierzu müssen
Sie einen Google-Account haben.
@divEnd


@node Helfen Sie uns
@unnumberedsec Helfen Sie uns
@translationof Help us

@divClass{column-center-top}
@helpusNeed

@divEnd

@divClass{column-left-top}
@divClass{keep-bullets}
@helpusTasks

@divEnd
@divEnd

@divClass{column-right-top}
@helpusProjects

@divEnd


@node Entwicklung
@unnumberedsec Entwicklung
@translationof Development

@divClass{heading-center}
@ifclear web_version
  @heading Entwicklung für LilyPond @version
@end ifclear
@ifset web_version
  @heading Entwicklung für LilyPond @versionDevel
@end ifset


@warning{Hier handelt es sich um nicht stabile Entwicklerversionen.
Wenn Sie nicht genau wissen, wie man LilyPond installiert und
benutzt, empfehlen wir ausdrücklich, die stabilen @ref{Download}-Versionen
zu benutzen und die stabilen @ref{Handbücher} zu lesen.}

@divEnd

@divClass{column-center-top}
@subheading Veröffentlichungsnummern

Es gibt zwei Veröffentlichungsreihen für LilyPond: stabile Versionen
und die unstabilen Entwicklerversionen.  Stabile Versionen haben eine
gerade zweite Versionsnummer (also etwa 2.8, 2.10, 2.12).
Entwicklerversionen haben eine ungerade zweite Versionsnummer
(also 2.7, 2.9, 2.11).

@divEnd


@divClass{column-left-top}
@subheading Download

Anleitungen zu git und der Kompilierung finden sich im Contributor's Guide (nur auf Englisch).

@quotation
@uref{http://git.sv.gnu.org/gitweb/?p=lilypond.git, lilypond git-Repositorium}
@end quotation

Schreiber der Dokumentation und Tester sollten die neuesten
Binärpakete herunterladen:

@quotation

@downloadDevelLinuxNormal

@downloadDevelLinuxBig

@downloadDevelLinuxPPC

@downloadDevelFreeBSDNormal

@downloadDevelFreeBSDBig

@downloadDevelDarwinNormal

@downloadDevelDarwinPPC

@downloadDevelWindows

@downloadDevelSource

@end quotation

@divEnd


@divClass{column-right-top}
@subheading Handbuch für Entwicklungsarbeiten (Contributor's guide)

Die Entwicklung von LilyPond ist eine ziemlich komplizierte Angelegenheit.
Um neuen Mitarbeitern zu helfen und das ganze System (ziemlich)
stabil zu halten, haben wir ein Handbuch für Entwicklungsarbeiten
geschrieben (nur auf Englisch).

@docLinksBare{Handbuch für Entwicklungsarbeiten, contributor,
  @rcontribnamed{Top,Handbuch für Entwicklungsarbeiten},
  @manualDevelContributorSplit-de,
  @manualDevelContributorBig-de, 500 kB,
  @manualDevelContributorPdf-de, 2.8 MB}

@divEnd


@divClass{column-center-top}
@subheading Regressionsteste

@divClass{keep-bullets}
@itemize

@ifclear web_version

@item
@uref{../../input/regression/collated-files.html, Regressionsteste}:
Die Regressionsteste dieser Version.
(@uref{../../input/regression/collated-files.pdf, PDF-Version})

@item
@uref{../../input/regression/musicxml/collated-files.html, MusicXML-Teste}:
musicXML-Regressionsteste dieser Version.
(@uref{../../input/regression/musicxml/collated-files.pdf, PDF-Version})
@end ifclear

@ifset web_version
@item @regtestDevel (@regtestDevelPdf{})

@item @regtestDevelXml (@regtestDevelXmlPdf{})

@item @regtestStable (@regtestStablePdf{})

@item @regtestStableXml (@regtestStableXmlPdf{})
@end ifset


@item @uref{http://lilypond.org/test, Archiv der Regressionsteste}:
Vergleiche zweier Versionen.

@end itemize
@divEnd
@divEnd



@divClass{column-center-bottom}
@subheading Handbücher

@ifclear web_version
@warning{Diese Handbücher sind für LilyPond @version{}; die neuesten
Handbücher finden sich auf @url{http://lilypond.org}.}
@end ifclear

@divClass{normal-table}
@multitable @columnfractions .3 .3 .3
@headitem Einleitung
@item
@docLinkSplit{Lernen,learning,@manualDevelLearningSplit-de}
@tab
@docLinkBig{Lernen,learning,@manualDevelLearningBig-de}
@tab
@docLinkPdf{Lernen,learning,@manualDevelLearningPdf-de}

@item
@docLinkSplit{Glossar,music-glossary,@manualDevelGlossarySplit-de}
@tab
@docLinkBig{Glossar,music-glossary,@manualDevelGlossaryBig-de}
@tab
@docLinkPdf{Glossar,music-glossary,@manualDevelGlossaryPdf-de}

@item
@docLinkSplit{Aufsatz,essay,@manualDevelEssaySplit-de}
@tab
@docLinkBig{Aufsatz,essay,@manualDevelEssayBig-de}
@tab
@docLinkPdf{Aufsatz,essay,@manualDevelEssayPdf-de}

@headitem Häufig benutzte Handbücher

@item
@docLinkSplit{Notation,notation,@manualDevelNotationSplit-de}
@tab
@docLinkBig{Notation,notation,@manualDevelNotationBig-de}
@tab
@docLinkPdf{Notation,notation,@manualDevelNotationPdf-de}

@item
@docLinkSplit{Benutzung,usage,@manualDevelUsageSplit-de}
@tab
@docLinkBig{Benutzung,usage,@manualDevelUsageBig-de}
@tab
@docLinkPdf{Benutzung,usage,@manualDevelUsagePdf-de}

@item
@docLinkSplit{Schnipsel,snippets,@manualDevelSnippetsSplit-de}
@tab
@docLinkBig{Schnipsel,snippets,@manualDevelSnippetsBig-de}
@tab
@docLinkPdf{Schnipsel,snippets,@manualDevelSnippetsPdf-de}


@headitem Seltenere Handbücher

@item
@docLinkSplit{Webseite,web,@manualDevelWebSplit-de}
@tab
@docLinkBig{Webseite,web,@manualDevelWebBig-de}
@tab
@docLinkPdf{Webseite,web,@manualDevelWebPdf-de}

@item
@docLinkSplit{Veränderungen,changes,@manualDevelChangesSplit-de}
@tab
@docLinkBig{Veränderungen,changes,@manualDevelChangesBig-de}
@tab
@docLinkPdf{Veränderungen,changes,@manualDevelChangesPdf-de}

@item
@docLinkSplit{Erweitern,extending,@manualDevelExtendingSplit-de}
@tab
@docLinkBig{Erweitern,extending,@manualDevelExtendingBig-de}
@tab
@docLinkPdf{Erweitern,extending,@manualDevelExtendingPdf-de}

@item
@docLinkSplit{Interna,internals,@manualDevelInternalsSplit-de}
@tab
@docLinkBig{Interna,internals,@manualDevelInternalsBig-de}
@tab
@docLinkPdf{Interna,internals,@manualDevelInternalsPdf-de}

@ifset web_version
@headitem Zum Herunterladen

@item
@doctarballDevel
@end ifset

@end multitable

@divEnd
@divEnd



@node Autoren
@unnumberedsec Autoren
@translationof Authors

@divClass{column-left-top}
@subheading Current Development Team

@divClass{keep-bullets}
@developersCurrent
@divEnd
@divEnd

@divClass{column-right-top}
@subheading Previous Development Team

@divClass{keep-bullets}
@developersPrevious
@divEnd
@divEnd


@divClass{column-center-top}
@subheading Current Contributors

@divClass{keep-bullets}
@subsubheading Programming

@coreCurrent

@subsubheading Font

@fontCurrent

@subsubheading Documentation

@docCurrent

@subsubheading Bug squad

@bugsquadCurrent

@subsubheading Support

@supportCurrent

@subsubheading Translation

@translationsCurrent

@divEnd
@divEnd


@divClass{column-center-bottom}
@subheading Previous Contributors

@divClass{keep-bullets}
@subsubheading Programming

@corePrevious

@subsubheading Font

@fontPrevious

@subsubheading Documentation

@docPrevious

@c    uncomment when we have any previous members -gp
@c @subsubheading Bug squad

@c @bugsquadCurrent

@subsubheading Support

@supportPrevious

@subsubheading Translation

@translationsPrevious

@divEnd
@divEnd



@node Veröffentlichungen
@unnumberedsec Veröffentlichungen
@translationof Publications

@divClass{column-center-top}
@subheading Was wir über LilyPond geschrieben haben

@divClass{keep-bullets}
@itemize

@item
Han-Wen Nienhuys, @emph{LilyPond, Automated music formatting and
the Art of Shipping}.  Forum Internacional Software Livre 2006
(FISL7.0) (@uref{http://lilypond.org/web/images/FISL7-slides.pdf,
PDF 1095k})

@item
Erik Sandberg, @emph{Separating input language and formatter in
GNU LilyPond}. Master's Thesis, Uppsala University, Department of
Information Technology March 2006.
(@uref{http://lilypond.org/web/images/thesis-erik-sandberg.pdf,
PDF 750k})

@item
Han-Wen Nienhuys and Jan Nieuwenhuizen, @emph{LilyPond, a system
for automated music engraving}.  Proceedings of the XIV Colloquium
on Musical Informatics (XIV CIM 2003), Firenze, Italy, May 2003.
(@uref{ http://lilypond.org/web/images/xivcim.pdf, PDF 95k})

@end itemize

@divEnd
@divEnd


@divClass{column-center-bottom}

@subheading Was andere mit LilyPond gemacht haben

@divClass{keep-bullets}
@itemize

@item
Graham Percival, Tosten Anders und George Tzanetakis,
@emph{Generating Targeted Rhythmic Exercises for Music Students
with Constraint Satisfaction Programming}, International Computer
Music Conference 2008.

@item
Alexandre Tachard Passos, Marcos Sampaio, Pedro Kröger, Givaldo de Cidra,
@emph{Functional Harmonic Analysis and Computational Musicology
in Rameau}, Proceedings of the 12th Brazilian Symposium on Computer
Music, 2009, pp. 207-210.

@item
Alberto Simões, Anália Lourenço and José João Almeida,
@emph{Using Text Mining Techniques for Classical Music Scores Analysis},
New Trends in Artificial Intelligence, 2007 J. Neves et al ed.

@item
Kevin C. Baird 2005,
@emph{Real-time generation of music notation via audience interaction using
python and GNU lilypond}. Proceedings of the 2005 Conference on New interfaces
For Musical Expression (Vancouver, Canada, May 26 - 28, 2005).


@end itemize

@divEnd
@divEnd


@node Ältere Neuigkeiten
@unnumberedsec Ältere Neuigkeiten
@translationof Old news

@include web/news-front.itexi

@include web/news.itexi


