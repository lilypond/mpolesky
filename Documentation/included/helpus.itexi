@c -*- coding: utf-8; mode: texinfo; -*-
@c This file is part of web/community.itexi and
@c contributor/introduction.itely

@macro helpusNeed
@subheading We need you!

The LilyPond development team is quite small; we really want to
get more people involved.  Please consider helping your fellow
LilyPond users by contributing to development!

Even working on small tasks can have a big impact: taking care
of them let experienced developers work on advanced tasks, instead
of spending time on those simple tasks.

@end macro


@macro helpusTasks
@subheading Simple tasks

@itemize
@item
Mailing list support: answer questions from fellow users.

@item
Bug reporting: help users create proper @rweb{Bug reports}, and/or
join the Bug Squad to organize @rcontrib{Issues}.

@item
Documentation: small changes can be proposed by following the
guidelines for @rcontrib{Documentation suggestions}.

@item
LilyPond Snippet Repository (LSR): create and fix snippets
following the guidelines in
@rcontrib{Adding and editing snippets}.

@item
Discussions, reviews, and testing: the developers often ask for
feedback about new documentation, potential syntax changes, and
testing new features.  Please contribute to these discussions!

@end itemize

@subheading Moderate tasks

@warning{These jobs generally require that you have the program
and documentation source files, but do not require a full
development environment.  See
@rcontrib{Working with source code}.}

@itemize
@item
Documentation: see
@rcontrib{Documentation work}, and
@rcontrib{Building documentation without compiling}.

@item
Website: the website is built from the normal documentation
source.  See the info about documentation, and also
@rcontrib{Website work}.

@item
Translations: see @rcontrib{Translating the documentation}, and
@rcontrib{Translating the website}.

@end itemize


@subheading Complex tasks

@warning{These jobs generally require that you have the source
code and can compile LilyPond.  See
@rcontrib{Working with source code},
and @rcontrib{Compiling}.
@*@*
We suggest that new contributors using Windows or MacOS X do
@strong{not} attempt to set up their own development environment;
instead, see
@rcontrib{Lilybuntu}.
}

@itemize
@item
Bugfixes, new features: the best way to begin is to join the
Frogs, and read @rcontrib{Programming work}.

@end itemize

@end macro


@macro helpusProjects
@subheading Projects

@subsubheading Frogs

Website and mailing list:

@example
@uref{http://frogs.lilynet.net}
@end example

The Frogs are ordinary LilyPond users who have chosen to get
involved in their favorite software's development.  Fixing bugs,
implementing new features, documenting the source code: there's a
lot to be done, but most importantly: this is a chance for
everyone to learn more about LilyPond, about Free Software, about
programming... and to have fun.  If you're curious about any of
it, then the word is: @emph{Join the Frogs!}



@subsubheading Grand LilyPond Input Syntax Standardization

Website:

@example
@uref{http://lilypond.org/~graham/gliss}
@end example

GLISS will stabilize the (non-tweak) input syntax for the upcoming
LilyPond 3.0.  After updating to 3.0, the input syntax for
untweaked music will remain stable for the foreseeable future.

We will have an extensive discussion period to determine the final
input specification.

@warning{GLISS will start shortly after 2.14 is released.}

@subsubheading Grand Organizing Project

Website:

@example
@uref{http://lilypond.org/~graham/gop}
@end example

GOP will be our big recruiting drive for new contributors.  We
desperately need to spread the development duties (including
@qq{simple tasks} which require no programming or interaction with
source code!) over more people.  We also need to document
knowledge from existing developers so that it does not get lost.

Unlike most @qq{Grand Projects}, GOP is not about adding huge new
features or completely redesigning things.  Rather, it is aimed at
giving us a much more stable foundation so that we can move ahead
with larger tasks in the future.

@warning{GOP will start shortly before or after the 2.14
release.}

@end macro


