%% Do not edit this file; it is automatically
%% generated from LSR http://lsr.dsi.unimi.it
%% This file is in the public domain.
\version "2.13.31"

\header {
  lsrtags = "rhythms"

%% Translation of GIT committish: 0b55335aeca1de539bf1125b717e0c21bb6fa31b
  texidoces = "
Normalmente están prohibidos los saltos de línea si las barras
atraviesan las líneas divisorias.  Se puede cambiar este
comportamiento como se muestra aquí:

"
  doctitlees = "Barras que atraviesan saltos de línea"


%% Translation of GIT committish: 0a868be38a775ecb1ef935b079000cebbc64de40
texidocde = "
Zeilenumbrüche sind normalerweise während Balken verboten.  Das kann geändert
werden.

"
  doctitlede = "Balken über Zeilenumbrüche"



%% Translation of GIT committish: 708e0d9c660856b19929736ca18a6f9d68539c60

  texidocfr = "
Il est normalement impensable qu'un saut de ligne tombe au milieu d'une
ligature.  LilyPond permet néanmoins de l'obtenir.

"
  doctitlefr = "Ligature au moment d'un saut de ligne"

  texidoc = "
Line breaks are normally forbidden when beams cross bar lines. This
behavior can be changed as shown:

"
  doctitle = "Beams across line breaks"
} % begin verbatim

\relative c'' {
  \override Beam #'breakable = ##t
  c8 c[ c] c[ c] c[ c] c[ \break
  c8] c[ c] c[ c] c[ c] c
}


