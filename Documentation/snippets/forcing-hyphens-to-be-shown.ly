%% Do not edit this file; it is automatically
%% generated from LSR http://lsr.dsi.unimi.it
%% This file is in the public domain.
\version "2.13.31"

\header {
  lsrtags = "vocal-music"

%% Translation of GIT committish: 0b55335aeca1de539bf1125b717e0c21bb6fa31b
  texidoces = "

Si LilyPond no cree que haya sitio suficiente para un guión separador
de sílabas, lo omitirá.  Se puede sobreescribir este comportamiento
con la propiedad @code{minimum-distance} de @code{LyricHyphen}.

"
  doctitlees = "Forzar la visibilidad de los guiones separadores de sílabas"



  texidoc = "
If LilyPond does not think there is space for a hyphen, it will be
omitted.  The behaviour can be overridden with the
@code{minimum-distance} property of @code{LyricHyphen}.

"
  doctitle = "Forcing hyphens to be shown"
} % begin verbatim

\relative c'' {
  c32 c c c
  c32 c c c
  c32 c c c
  c32 c c c
}
\addlyrics {
  syl -- lab word word
  \override LyricHyphen #'minimum-distance = #1.0
  syl -- lab word word
  \override LyricHyphen #'minimum-distance = #2.0
  syl -- lab word word
  \revert LyricHyphen #'minimum-distance
  syl -- lab word word
}
