%% Do not edit this file; it is automatically
%% generated from LSR http://lsr.dsi.unimi.it
%% This file is in the public domain.
\version "2.13.31"

\header {
  lsrtags = "expressive-marks, tweaks-and-overrides"

%% Translation of GIT committish: 0b55335aeca1de539bf1125b717e0c21bb6fa31b
  texidoces = "
Se puede ajustar la posición vertical de una ligadura de
expresiónutilizando la propiedad @code{positions} del objeto
@code{Slur}.  La propiedad tiene dos parámetros, refiriéndose el
primero al extremo izquierdo de la ligadura y el segundo al derecho.
Los valores de los parámetros no se utilizan por parte de LilyPond
para producir un desplazamiento exacto de la ligadura: más bien
selecciona la colocación que mejor aspecto tiene, teniendoo en cuenta
los valores de los parámetros.  Los valores positivos desplazan la
ligadura hacia arriba, y son adecuados a notas que tienen las plicas
hacia abajo.  Los valores negativos desplazan la ligadura hacia abajo.

"
  doctitlees = "Desplazar ligaduras de expresión verticalmente"



  texidoc = "
The vertical position of a slur can be adjusted using the
@code{positions} property of @code{Slur}.  The property has 2
parameters, the first referring to the left end of the slur and the
second to the right.  The values of the parameters are not used by
LilyPond to make an exact movement of the slur - instead it selects
what placement of the slur looks best, taking into account the
parameter values.  Positive values move the slur up, and are
appropriate for notes with stems down.  Negative values move the slur
down.

"
  doctitle = "Moving slur positions vertically"
} % begin verbatim

\relative c' {
  \stemDown
  e4( a)
  \override Slur #'positions = #'(1 . 1)
  e4( a)
  \override Slur #'positions = #'(2 . 2)
  e4( a)
  \override Slur #'positions = #'(3 . 3)
  e4( a)
  \override Slur #'positions = #'(4 . 4)
  e4( a)
  \override Slur #'positions = #'(5 . 5)
  e4( a)
  \override Slur #'positions = #'(0 . 5)
  e4( a)
  \override Slur #'positions = #'(5 . 0)
  e4( a)
}

