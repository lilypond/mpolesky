\version "2.13.4"

\header {
  lsrtags = "rhythms"
  texidoc = "
Beaming patterns may be altered with the @code{beatGrouping} property:

"
  doctitle = "Grouping beats"
}

\markup {
  This snippet is deprecated as of version 2.13.5 and will be removed
  in version 2.14.
}
