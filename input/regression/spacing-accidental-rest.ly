\version "2.13.25"

\header {
  texidoc = "Accidentals don't collide with shifted-down rests."
}

\new Staff << g'4 \\ {r8 aeses} >>

