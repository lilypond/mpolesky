\version "2.13.31"

\header {

  texidoc = "
Long titles should be properly centered.

"

  title = \markup \center-column {
    "How Razorback Jumping Frogs Level Six Piqued Gymnasts"
  }
}

\score {
  s1
}
