\version "2.13.24"

\header {
  texidoc = "The dots on a dotted rest are correctly accounted for
in horizontal spacing."
}

{ r16. cis'' }
