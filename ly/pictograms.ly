\version "2.13.11"

%{
\markup \override #'(baseline-skip . 0) \column {
     \concat { \template-rectangle \template-rectangle \template-rectangle }
     \concat { \template-rectangle \conga \template-rectangle }
     \concat { \template-rectangle \template-rectangle \template-rectangle }
     \concat { \template-rectangle \template-polygon \template-circles \template-mixed }
}

%}

%{

% ossia stuff

\new Staff = main \relative c'' {
  c4^\markup \crotale b d^\markup \fontsize #2 \crotale c
  <<
    { c4 b d c }

    \new Staff \with {
      \remove "Time_signature_engraver"
      alignAboveContext = #"main"
      fontSize = #-3
      \override StaffSymbol #'staff-space = #(magstep -3)
      \override StaffSymbol #'thickness = #(magstep -3)
      firstClef = ##f
    }
    { e4^\markup \crotale d f e }
  >>
  c4 b c2
}

%}

\markup \column {
  \score {
    \new RhythmicStaff \with {
      %instrumentName = \markup \triangle-a
      \remove Time_signature_engraver
      \consists Clef_engraver
    } {
      \clef percussion
      c^\markup \crotale
    }
    \layout {}
  }
  \score {
    \new RhythmicStaff \with {
      instrumentName = \markup \triangle-a
      \remove Time_signature_engraver
      \consists Clef_engraver
    } {
      \clef percussion
      c%^\markup \crotale
    }
    \layout {}
  }
}

\markup \concat {
  %\wind-chimes
  \triangle-a \triangle-b \triangle-c
  \triangle-d \triangle-e \triangle-f
  \crotale \crotales \finger-cymbals
  \sistrum

  }

\markup \concat {
  \sleigh-bells-a %{ \sleigh-bells-b %} \cymbal-tongs
  \sizzle-cymbal \suspended-cymbal \crash-cymbals
  \brake-drum \domed-gong-a \domed-gong-b \gong
}

\markup \concat {
  \maraca \maracas \cabasa

}

\markup \concat {
  \conga
  \snare-a \snare-b
  \snare-off-a \military-drum \tenor-drum \tomtom \tomtom-wood-top
  \bass-drum-a
}
