(load "scheme2ps.scm")

(use-modules (ice-9 pretty-print))

;; example implementation in ps:
;; { % triangle-e
;;   0 0 moveto
;;   7.2169 12.5 lineto
;;   14.4338 0 lineto
;;   closepath
;;   7.2169 4.1667 moveto
;;   14.4338 8.3333 lineto
;; } 0.16 0.08 1 draw_pictogram


;; can the same pictogram appear smaller in an ossia?

(define (pictogram layout props path-width path-height path)
  ;; path dimensions are measured in pen-diameter units.
  (let* ((pen-diameter (* 0.16 1)) ; when font-size is 0
         (scale-path 1)      ; without scaling pen-diameter
         (path-width-actual  (* path-width  scale-path))
         (path-height-actual (* path-height scale-path))

         (font-size (assoc-ref (car props) 'font-size))
         (scale-pen (magstep (if font-size font-size 0)))
         (pen-diameter-actual (* pen-diameter scale-pen))

         ;; for the draw_pictogram ps routine
         (ps-scale       (* pen-diameter-actual scale-path))
         (ps-path-offset (/ pen-diameter-actual 2))
         (ps-linewidth   (/ scale-path))

         ;; for ly:make-stencil
         (stencil-width
           (+ (* path-width-actual pen-diameter-actual)
              ;; stencil exceeds path by half
              ;; of pen-diameter on each side:
              pen-diameter-actual))
         (stencil-height
           (+ (* path-height-actual pen-diameter-actual)
              pen-diameter-actual)))

   (interpret-markup layout props
    (markup #:stencil
     (ly:make-stencil
      (list 'embedded-ps
       ;; send args to the draw_pictogram ps routine
       (string-append
        (ly:format "\n{~a} " path)
        (objects->ps-string ps-scale
                            ps-path-offset
                            ps-linewidth)
        "draw_pictogram\n"))
      (cons 0 stencil-width)
      (cons 0 stencil-height))))))



; %%%%%%%%%%%% TESTS %%%%%%%%%%%%

; % script characters %


; %%%%%%%%%%%% GLASS %%%%%%%%%%%%

(define-markup-command (test layout props) ()
  (pictogram layout props 1 1 "% test \n"))


(define question-mark
  (string-append
    (moveto 4.75 8)
    (arcn 6.25 8 1.5 180 -30)
    (arc 7.75 5.4019 1.5 120 180)
    (lineto 6.25 4.75)
    (moveto 6.25 3)
    (rlineto 0 0)))


(define-markup-command (template-rectangle layout props) ()
  (let* ((width 12.5)
         (height width)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% template-rectangle:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)
     question-mark))))


(define-markup-command (template-polygon layout props) ()
  (let* ((width 12.5)
         (height width)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (P0 (cons 2.5101 0))
         (P1 (cons 0 7.7254))
         (P2 (cons 6.5716 12.5))
         (P3 (cons 13.1433 7.7254))
         (P4 (cons 10.6331 0))
         (orig-convex-hull (list P0 P1 P2 P3 P4))
         (rotated-hull
           (rotate-points rotate-img orig-convex-hull))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% template-polygon:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0)
     (lineto P1 P2 P3 P4)
     (closepath)
     question-mark))))


(define-markup-command (template-circles layout props) ()
  (let* ((width 12.5)
         (height width)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (radius 4)
         (left-x radius)
         (right-x (- width radius))
         (top-y (- height radius))
         (bottom-y radius)
         (top-left (cons left-x top-y))
         (top-right (cons right-x top-y))
         (bottom-right (cons right-x bottom-y))
         (bottom-left (cons left-x bottom-y))
         (circles (list (cons top-left radius)
                        (cons top-right radius)
                        (cons bottom-right radius)
                        (cons bottom-left radius)))
         (rotated-circle-bboxes
           (get-rotated-circle-bboxes rotate-img circles))
         (rotated-exts (get-bbox-extents rotated-circle-bboxes))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% template-circles:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto 0 top-y)
     (arcn top-left radius 180 90)
     (arcn top-right radius 90 0)
     (arcn bottom-right radius 360 270)
     (arcn bottom-left radius 270 180)
     (closepath)
     question-mark))))


(define-markup-command (template-mixed layout props) ()
  (let* ((width 12.5)
         (height width)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (P0 (cons 0 0))
         (P1 (cons width height))
         (radius 4)
         (left-x radius)
         (right-x (- width radius))
         (top-y (- height radius))
         (bottom-y radius)
         (top-left (cons left-x top-y))
         (bottom-right (cons right-x bottom-y))
         (circles (list (cons top-left radius)
                        (cons bottom-right radius)))
         (orig-convex-hull (list P0 P1))
         (rotated-circle-bboxes
           (get-rotated-circle-bboxes rotate-img circles))
         (rotated-hull
           (append (rotate-points rotate-img orig-convex-hull)
                   rotated-circle-bboxes))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% template-mixed:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0)
     (arcn top-left radius 180 90)
     (lineto P1)
     (arcn bottom-right radius 360 270)
     (closepath)
     question-mark))))

(define-markup-command (wind-chimes layout props) ()
  (let* ((width 12.5)
         (height width)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;

         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% wind-chimes:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 12.5 12.5)
     question-mark
     ))))


; %%%%%%%%%%%% METALS %%%%%%%%%%%


(define
  (triangle-generic
    layout
    props
    full-side-length
    open-side-length
    beater-specs ; '(length angle inside-endpoint) or #f
    rotate-img
    flip-img)
  (let* ((height (* full-side-length (/ (sqrt 3) 2)))
         (width (+ open-side-length
                   (/ (- full-side-length open-side-length) 2)))
         (x-at-apex (- width (/ full-side-length 2)))

         (open/full-ratio (/ open-side-length full-side-length))
         (open-endpoints-dx (- width open-side-length))
         (open-endpoints-dy (* height (- 1 open/full-ratio)))

         (P0 (cons open-endpoints-dx 0))
         (P1 (cons width 0))
         (P2 (cons x-at-apex height))
         (P3 (cons 0 open-endpoints-dy))

         (get-beater-spec
           (lambda (accessor)
             (if beater-specs (accessor beater-specs))))

         ;; beater specs
         (beater-length (get-beater-spec car))
         (beater-angle (get-beater-spec cadr))
         (beater-inside-endpoint (get-beater-spec caddr))

         (beater-outside-endpoint
           (if beater-specs
               (cons (+ (car beater-inside-endpoint)
                        (* beater-length (cos-deg beater-angle)))
                     (+ (cdr beater-inside-endpoint)
                        (* beater-length (sin-deg beater-angle))))
               #f))

         (orig-convex-hull
           (if beater-specs
               (list P0 P1 P2 P3 beater-outside-endpoint)
               (list P0 P1 P2 P3)))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% triangle:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0)
     (lineto P1 P2)
     (if (= full-side-length open-side-length)
         (closepath)
         (lineto P3))
     (if beater-specs
         (string-append (moveto beater-inside-endpoint)
                        (lineto beater-outside-endpoint))
         "")))))

(define-markup-command (triangle-a layout props) ()
  (triangle-generic layout
                    props
                    14.4338
                    11.2583
                    #f
                    0
                    #f))

(define-markup-command (triangle-b layout props) ()
  (triangle-generic layout
                    props
                    14.4338
                    11.2583
                    (list 8.3333 30 (cons 5.6292 4.1667))
                    0
                    #f))

(define-markup-command (triangle-c layout props) ()
  (triangle-generic layout
                    props
                    14.4338
                    11.2583
                    (list 9.4871 60 (cons 7.8994 2.75))
                    0
                    #f))

(define-markup-command (triangle-d layout props) ()
  (triangle-generic layout
                    props
                    14.4338
                    14.4338
                    #f
                    0
                    #f))

(define-markup-command (triangle-e layout props) ()
  (triangle-generic layout
                    props
                    14.4338
                    14.4338
                    (list 8.3333 30 (cons 7.2169 4.1667))
                    0
                    #f))

(define-markup-command (triangle-f layout props) ()
  (triangle-generic layout
                    props
                    14.4338
                    14.4338
                    (list 9.4871 60 (cons 9.5984 2.75))
                    0
                    #f))


(define-markup-command (crotale layout props) ()
  (let* ((width 12.5)
         (height 4.5)
         (crotale-height 2)
         (dome-width 3)
         (dome-height 1)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (x-center (/ width 2))
         (x1 (/ (- width dome-width) 2))
         (x2 (- width x1))
         (orig-convex-hull (list 0 0
                                 0 crotale-height
                                 x-center height
                                 width crotale-height
                                 width 0))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% crotale:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto 0 0)
     (lineto 0 crotale-height)
     (rlineto (/ (- width dome-width) 2) 0)
     (arch-curveto-factor (cons x1 crotale-height)
                          (cons x2 crotale-height)
                          dome-height
                          2/3)
     (lineto width crotale-height width 0)
     (closepath)
     (moveto x-center height)
     (lineto x-center (+ crotale-height dome-height))))))


(define-markup-command (crotales layout props) ()
  (let* ((width 24)
         (height 6.25)
         (crotale-width 10)
         (crotale-height 2)
         (pin-height 2)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;

         ;;      2
         ;;  1---*---3
         ;;  0-------*
         ;;                4
         ;;            *---*---5
         ;;  .         7-------6

         (pin-x (/ crotale-width 2))
         (dx (- width crotale-width))
         (dy (- height pin-height crotale-height))

         (P0 (cons 0 dy))
         (P1 (cons 0 (+ (cdr P0) crotale-height)))
         (P2 (cons pin-x height))
         (P3 (cons crotale-width (cdr P1)))

         (P4 (cons (- width pin-x) (+ crotale-height pin-height)))
         (P5 (cons width crotale-height))
         (P6 (cons width 0))
         (P7 (cons dx 0))

         (orig-convex-hull (list P0 P1 P2 P3 P4 P5 P6 P7))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))

   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
     (string-append
      "% crotales:\n"
      (rotate-and-flip-image rotate-img flip-img rotated-exts)
      (objects->ps-string dx (- dy) 0 dy)
      "\n"
      (repeat 2 (bind (translate)
                      (rectstroke 0 0 crotale-width crotale-height)
                      (moveto pin-x crotale-height)
                      (rlineto 0 pin-height)))))))


(define-markup-command (finger-cymbals layout props) ()
  (let* ((height 12.5)
         (radius 2)
         (separation 2.5)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (y-center (/ height 2))
         (x2 (+ radius separation))
         (width (+ separation (* 2 radius)))
         (orig-convex-hull (list radius 0
                                 radius height
                                 x2 height
                                 x2 0))
         (circ0-center (cons radius y-center))
         (circ1-center (cons x2 y-center))
         (rotated-exts (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-circle-bbox
           (get-rotated-circle-bboxes
              rotate-img
              `(,(cons circ0-center radius)
                ,(cons circ1-center radius))))
         (rotated-hull
           (append (rotate-points rotate-img orig-convex-hull)
                   rotated-circle-bbox))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
    (pictogram layout props (car rotated-dims) (cdr rotated-dims)
     (string-append
      "% finger-cymbals:\n"
      (rotate-and-flip-image rotate-img flip-img rotated-exts)
      (translate (/ width 2) y-center)
      (repeat 2 (bind (moveto (/ separation -2) (- y-center))
                      (rlineto 0 height)
                      (stroke)
                      (arc (/ separation -2) 0 radius 90 270)
                      (rotate 180)))))))

;; TODO: fix arg values
(define-markup-command (sistrum layout props) ()
  (let* ((width 12.5)
         (height 25)
         (pin-height 22)
         (U-bar-width 10.5)
         (U-bar-height 12.5)
         (jingle-separation 2)
         (jingle-height 5)
         (jingles 4)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (x1 (/ (- width U-bar-width) 2))
         (x-center (/ width 2))
         (x2 (- x-center (* jingle-separation (+ jingles 1) 1/2)))
         (x4 (- width x1))
         (dy (* (- height pin-height U-bar-height) 4/3))
         (orig-convex-hull (list x-center 0
                                 0 pin-height
                                 x1 height
                                 x4 height
                                 width pin-height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))

   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% sistrum:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto 0 pin-height)
     (rlineto width 0)
     (moveto x1 height)
     (rlineto 0 (- pin-height height))
     (rcurveto 0 dy U-bar-width dy U-bar-width 0)
     (rlineto 0 (- height pin-height))
     (moveto x-center (- height U-bar-height))
     (lineto x-center 0)
     (moveto x2 (- pin-height (/ jingle-height 2)))
     (repeat jingles (bind (rmoveto jingle-separation jingle-height)
                     (rlineto 0 (- jingle-height))))))))


(define-markup-command (sleigh-bells-a layout props) ()
  (let* ((width 26)
         (radius 1.72)
         (bell-separation-x 0)
         (bell-separation-y 0.39)
         (columns 3)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (height (+ (* radius 4) bell-separation-y))
         (y-center (/ height 2))
         (bell-distance-x (+ (* radius 2) bell-separation-x))
         (bell-distance-y (+ (* radius 2) bell-separation-y))

         ;; circle centers (x's and y's)
         (right-x (- width radius))
         (top-y (- height radius))
         (bottom-y radius)
         (left-x (- right-x
                    (* (- columns 1)
                       bell-distance-x)))

         ;; circle centers (coordinate pairs)
         (top-left (cons left-x top-y))
         (top-right (cons right-x top-y))
         (bottom-right (cons right-x bottom-y))
         (bottom-left (cons left-x bottom-y))

         (circles (list (cons top-left radius)
                        (cons top-right radius)
                        (cons bottom-right radius)
                        (cons bottom-left radius)))
         (orig-convex-hull (list 0 y-center))
         (rotated-circle-bboxes
           (get-rotated-circle-bboxes rotate-img circles))
         (rotated-hull
           (append (rotate-points rotate-img orig-convex-hull)
                   rotated-circle-bboxes))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% sleigh-bells-a:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto 0 y-center)
     (rlineto right-x 0)
     (translate (- width (* bell-distance-x columns))
                (- y-center (/ bell-distance-y 2)))
     (repeat
       columns
       (bind (objects->ps-string
               0
               (- bell-distance-y)
               bell-distance-x
               bell-distance-y)
               (repeat
                 2
                 (translate)
                 (moveto 0 0)
                 (arc (- radius) 0 radius 0 360))))))))

; (define-markup-command (sleigh-bells-b layout props) ()
;   ;; TODO: fix this.
;   (let* ((r1 3.5) (r2 6) (n 7)
;          (rotate-img 0) (flip-img #f) ; 'x 'y or #f
;          ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;          (a (/ 180 (- n 1))) (ar (* a (angle -1) 1/180))
;          (r3 (/ r2 (- (* 2 (/ (cos ar) (sin ar))) 1)))
;          (width (+ (* 2 r2) (* 4 r3))) (height (* 2 (+ r2 r3)))
;          (convex-hull (rect-hull width height))
;          (rotated-hull (rotate-points rotate-img convex-hull))
;          (rotated-dims (get-bbox-dimensions rotated-hull)))
;    (pictogram layout props width height (float-format "% sleigh-bells-b:
; %   1 2 scale
; 0 0 moveto
; ~a ~a translate
; ~a ~a 2 {dup 0 exch moveto 0 0 3 -1 roll -270 90 arc} bind repeat
; 0 0 moveto ~a rotate
; ~a {0 0 moveto ~a rotate 0 ~a moveto 0 ~a ~a -90 270 arc} bind repeat
; "
; `(,(/ width 2) ,(- height r2)
; ,r2 ,r1
; ,(- 90 a)
; ,n ,a ,r2 ,(+ r2 r3) ,r3)))))

(define-markup-command (sleigh-bells-b layout props) ()
  ;; TODO: fix this.
  (let* ((r1 3.5) (r2 6) (n 7)
         (rotate-img 0) (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (a (/ 180 (- n 1))) (ar (* a (angle -1) 1/180))
         (r3 (/ r2 (- (* 2 (/ (cos ar) (sin ar))) 1)))
         (width (+ (* 2 r2) (* 4 r3))) (height (* 2 (+ r2 r3)))
         (convex-hull (rect-hull width height))
         (rotated-hull (rotate-points rotate-img convex-hull))
         (rotated-dims (get-bbox-dimensions rotated-hull)))
   (pictogram layout props width height (float-format "% sleigh-bells-b:
%   1 2 scale
0 0 moveto
~a ~a translate
~a ~a 2 {dup 0 exch moveto 0 0 3 -1 roll -270 90 arc} bind repeat
0 0 moveto ~a rotate
~a {0 0 moveto ~a rotate 0 ~a moveto 0 ~a ~a -90 270 arc} bind repeat
"
`(,(/ width 2) ,(- height r2)
,r2 ,r1
,(- 90 a)
,n ,a ,r2 ,(+ r2 r3) ,r3)))))



(define-markup-command (sleigh-bells-b layout props) ()
  (let* ((width 12.5)
         (height width)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (P0 (cons 0 0))
         (P1 (cons width height))
         (radius 4)
         (left-x radius)
         (right-x (- width radius))
         (top-y (- height radius))
         (bottom-y radius)
         (top-left (cons left-x top-y))
         (bottom-right (cons right-x bottom-y))
         (circles (list (cons top-left radius)
                        (cons bottom-right radius)))
         (orig-convex-hull (list P0 P1))
         (rotated-circle-bboxes
           (get-rotated-circle-bboxes rotate-img circles))
         (rotated-hull
           (append (rotate-points rotate-img orig-convex-hull)
                   rotated-circle-bboxes))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% sleigh-bells-b:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0)
     (arcn top-left radius 180 90)
     (lineto P1)
     (arcn bottom-right radius 360 270)
     (closepath)
     question-mark))))


(define-markup-command (cymbal-tongs layout props) ()
  (let* ((width 18)
         (height 6.25)
         (cymbal-width 5)
         (separation 2)
         (tong-angle 7.5)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         ;;
         ;;     1------______2 3
         ;;  9--0--*          \
         ;;                    4
         ;; 10--8--*   ______ /
         ;;     7------      6 5

         (y-center (/ height 2))
         (tong-width (- width (/ cymbal-width 2)))

         ;; height where line P7->P6 intersects line x=width
         (y0 (* tong-width (tan-deg (/ tong-angle 2))))

         (radius (/ (- y-center y0)
               (cos-deg tong-angle)))
         (P0 (cons (/ cymbal-width 2)
                   (/ (+ height separation) 2)))
         (P1 (cons (/ cymbal-width 2)
                   height))
         (P3 (cons width (- height y0)))
         (P4 (cons width y-center))
         (P5 (y-mirror P3 height))
         (P7 (y-mirror P1 height))
         (P8 (y-mirror P0 height))
         (P9 (cons 0 (/ (- height separation) 2)))
         (P10 (y-mirror P9 height))
         (occ (cons (- width radius)
                    y-center)) ; orig-circ-cntr
         (P2 (map-on-pairs + occ (vector-offset (- 90 (/ tong-angle 2)) radius)))
         (P6 (cons (car P2)
                   (- height (cdr P2))))
         (orig-convex-hull (list P1 P2 P4 P6 P7 P9 P10))
         (rotated-circle-bbox
           (get-rotated-circle-bboxes rotate-img `(,(cons occ radius))))
         (rotated-hull
           (append (rotate-points rotate-img orig-convex-hull)
                   rotated-circle-bbox))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))

   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
     (string-append
     "% cymbal-tongs:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0)
     (lineto P1)
     (arcto radius P3 P5 P7)
     (lineto P7 P8)
     (moveto P9)
     (rlineto cymbal-width 0)
     (moveto P10)
     (rlineto cymbal-width 0)))))


;; TODO: check arg values.
(define-markup-command (sizzle-cymbal layout props) ()
  (let* ((width 12.5)
         (height 4)
         (rivet-height 1.75)
         (edge-separation 1)
         (rivet-separation 1.5)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (x-center (/ width 2))
         (cymbal-height (/ rivet-height 2))

         ;;           0
         ;;     3 7   |   8 5
         ;;   1-+-+---*---+-+-2
         ;;     4 *       * 6

         (P0 (cons x-center height))
         (P1 (cons 0 cymbal-height))
         (P2 (cons width cymbal-height))
         (P3 (cons edge-separation rivet-height))
         (P4 (cons edge-separation 0))
         (P5 (x-mirror P3 width))
         (P6 (x-mirror P4 width))
         (P7 (cons (+ edge-separation rivet-separation)
                   rivet-height))
         (P8 (x-mirror P7 width))
         (orig-convex-hull (list P0 P1 P2 P3 P4 P5 P6))
         (rotated-hull
           (rotate-points rotate-img orig-convex-hull))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% sizzle-cymbal:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0) (lineto x-center cymbal-height)
     (moveto P1) (lineto P2)
     (moveto P3) (lineto P4)
     (moveto P5) (lineto P6)
     (moveto P7) (rlineto 0 (- rivet-height))
     (moveto P8) (rlineto 0 (- rivet-height))))))


(define-markup-command (suspended-cymbal layout props) ()
  (let* ((width 12.5)
         (height 3.125)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;

         ;;       0
         ;;   1---*---2

         (P0 (cons (/ width 2) height))
         (P1 (cons 0 0))
         (P2 (cons width 0))

         (orig-convex-hull (list P0 P1 P2))
         (rotated-hull
           (rotate-points rotate-img orig-convex-hull))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% suspended-cymbal:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0)
     (rlineto 0 (- height))
     (moveto P1)
     (lineto P2)))))


(define-markup-command (crash-cymbals layout props) ()
  (let* ((height 12.5) (strap 2) (sep 2.5) (y-center (/ height 2))
         (spin 0) (flip #f) ; rotate and flip pictogram
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (convex-hull (rect-hull width height))
         (rotated-hull (rotate-points spin convex-hull))
         (rotated-dims (get-bbox-dimensions rotated-hull)))
   (pictogram layout props (+ (* 2 strap) sep) height
   (float-format "% crash-cymbals:
~a ~a 2 {0 moveto 0 ~a rlineto} repeat
~a 0 2 {~a moveto ~a 0 rlineto} repeat"
`(,(+ strap sep) ,strap height
  ,(+ strap sep) ,yc ,strap)))))


(define-markup-command (crash-cymbals layout props) ()
  (let* ((height 12.5)
         (strap-width 2)
         (separation 2.5)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;

         (width (+ (* strap-width 2) separation))
         (y-center (/ height 2))
         (P0 (cons strap-width height))
         (P1 (cons strap-width 0))
         (P2 (x-mirror P0 width))
         (P3 (x-mirror P1 width))
         (P4 (cons 0 y-center))
         (P5 (cons width y-center))

         (orig-convex-hull (list P0 P1 P2 P3 P4 P5))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% crash-cymbals:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0)
     (lineto P1)
     (moveto P2)
     (lineto P3)
     (moveto P4)
     (rlineto strap-width 0)
     (moveto P5)
     (rlineto (- strap-width) 0)))))


(define-markup-command (high-hat layout props) ()
  (let* ((width 12.5) (height 17) (lch 11.5) (uch 14) ; width height lower-cymb-h upper-cymb-h
         (x-center (/ width 2))
         (spin 0) (flip #f) ; rotate and flip pictogram
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (convex-hull (rect-hull width height))
         (rotated-hull (rotate-points spin convex-hull))
         (rotated-dims (get-bbox-dimensions rotated-hull)))
   (pictogram layout props width height (float-format "% high-hat:
0 ~a ~a ~a 0 ~a ~a ~a ~a 0 0 ~a ~a 0 0 ~a
4 {moveto rlineto} bind repeat"
`(,(- lch) ,xc ,lch ,(- uch height) ,xc height width ,lch width ,uch)))))

(define-markup-command (cowbell layout props) ()
  (let* ((width 11.5) (height 15.5) (uw 7) (bh 12.5) (rw 4) ; width height upper-w base-w ring-w
         (x0 (/ (- width uw) 2)) (x1 (/ (- width rw) 2)) (y2 (* (- height bh) 4/3))
         (spin 0) (flip #f) ; rotate and flip pictogram
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (convex-hull (rect-hull width height))
         (rotated-hull (rotate-points spin convex-hull))
         (rotated-dims (get-bbox-dimensions rotated-hull)))
   (pictogram layout props width height (float-format "% cowbell:
0 ~a ~a ~a ~a 0 ~a ~a ~a ~a ~a 0 ~a ~a 0 0
moveto 3 {rlineto} repeat closepath moveto rcurveto"
`(,y2 ,rw ,y2 ,rw ,x1 ,bh ,x0 ,(- bh) ,uw ,x0 ,bh)))))

(define-markup-command (herd-bell layout props) ()
  (let* ((width 11.5)   (height 15.5)  (bw 6.5) ; width           height      base-w
         (hwp 6.75) (bh 12.5) (rw 4)   ; h@widest-pt bell-h ring-w

         (j 0.4477) (k (- 1 j)) ; j=(7-4*rt2)/3
         (h-hwp (- height hwp)) (rr (/ rw 2))
         (x2 (/ (- width bw) 2)) (x1 (* x2 k))
         (x3 (/ (- width rw) 2)) (x4 (- width x3)) (x7 (- width x1))
         (y2 (* hwp (+ 1 j) 1/2)) (y1 (* hwp k 1/2)) (y4 (* (- bh hwp) 4/3))
         (spin 0) (flip #f) ; rotate and flip pictogram
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (convex-hull (rect-hull width height))
         (rotated-hull (rotate-points spin convex-hull))
         (rotated-dims (get-bbox-dimensions rotated-hull)))
   (pictogram layout props width height (float-format "% herd-bell:
0 ~a moveto
0 ~a ~a ~a ~a 0 curveto
~a 0 rlineto
~a ~a ~a ~a ~a ~a curveto
stroke
newpath
0 ~a moveto
0 ~a ~a ~a ~a 0 rcurveto
gsave stroke grestore
0 ~a ~a 0 0 ~a 0.5 0 4 {rlineto} repeat
closepath
clip
newpath
~a ~a moveto
~a ~a ~a ~a ~a arct
~a ~a ~a ~a ~a arct
~a ~a lineto"
`(,hwp   ,y2 ,x1 ,y1 ,x2   ,bw   ,x7 ,y1 width ,y2 width ,hwp   ,hwp   ,y4 width ,y4 width
,(- (+ h-hwp 1/2)) ,(- (+ width 1)) ,(+ h-hwp 1/2)
,x3 ,hwp   ,x3 height ,x4 height ,rr   ,x4 height ,x4 ,bh ,rr   ,x4 ,hwp)))))

(define-markup-command (bell-plate layout props) ()
  (let* ((width 10) (height 18) (ph 15) (dx 2) (dy 3) ; plate-h hole/corner-dist-x -y
         (spin 0) (flip #f) ; rotate and flip pictogram
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (convex-hull (rect-hull width height))
         (rotated-hull (rotate-points spin convex-hull))
         (rotated-dims (get-bbox-dimensions rotated-hull)))
   (pictogram layout props width height (float-format "% bell-plate:
0 0 ~a ~a rectstroke ~a ~a 2 {~a moveto 0 ~a rlineto} repeat"
`(width ,ph ,(- width dx) ,dx ,(- ph dy) ,(- height (- ph dy)))))))

(define-markup-command (bell-a layout props) ()
  (let* ((width 12.5) (height width)
         (spin 0) (flip #f) ; rotate and flip pictogram
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (convex-hull (rect-hull width height))
         (rotated-hull (rotate-points spin convex-hull))
         (rotated-dims (get-bbox-dimensions rotated-hull)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% bell-a:\n"
    ;   (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)))))

(define-markup-command (bell-b layout props) ()
  (let* ((width 12.5) (height width)
         (a 0) ; rotate-pictogram angle [0,360]
         (flip #f) ; flip-pictogram: 'x, 'y, or #f.
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         )
   (pictogram layout props (rotated-rect-w width height a) (rotated-rect-h width height a)
    (string-append
     "% bell-b:\n"
    ;   (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)))))

(define-markup-command (handbell layout props) ()
  (let* ((width 12.5) (height width)
         (a 0) ; rotate-pictogram angle [0,360]
         (flip #f) ; flip-pictogram: 'x, 'y, or #f.
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         )
   (pictogram layout props (rotated-rect-w width height a) (rotated-rect-h width height a)
    (string-append
     "% handbell:\n"
    ;   (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)))))

(define-markup-command (flexatone layout props) ()
  (let* ((width 12.5) (height width)
         (a 0) ; rotate-pictogram angle [0,360]
         (flip #f) ; flip-pictogram: 'x, 'y, or #f.
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         )
   (pictogram layout props (rotated-rect-w width height a) (rotated-rect-h width height a)
    (string-append
     "% flexatone:\n"
    ;   (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)))))

(define-markup-command (chinese-cymbal layout props) ()
  (let* ((width 12.5) (height 4) (r 2))
   (pictogram layout props width height (float-format "% chinese-cymbal:
0 ~a moveto 0 0 ~a 0 ~a arct ~a 0 ~a ~a ~a arct ~a 0 moveto 0 ~a rlineto"
`(,r width ,r width width ,r ,r ,(/ width 2) height)))))

(define-markup-command (vietnamese-hat layout props) ()
  (let* ((width 16) (height 10.5) (y1 8))
   (pictogram layout props width height (float-format "% vietnamese-hat:
0 0 moveto ~a ~a lineto ~a 0 lineto ~a ~a moveto 0 ~a rlineto"
`(,(/ width 2) ,y1 width ,(/ width 2) height ,(- y1 height))))))


(define-markup-command (brake-drum layout props) ()
  (let* ((outer-radius 6)
         (inner-radius 2)
         (screw-radius 0.375)
         (screw-holes 4)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (width (* outer-radius 2))
         (height width)
         (screw-orbit (/ (+ inner-radius outer-radius) 2))
         (screw-angle (/ 360 screw-holes))
         (center (cons outer-radius outer-radius))
         (P0 (cons width outer-radius))
         (P1 (cons (+ outer-radius inner-radius) outer-radius))

         (circles (list (cons center outer-radius)))
         (rotated-circle-bboxes
           (get-rotated-circle-bboxes rotate-img circles))
         (rotated-exts (get-bbox-extents rotated-circle-bboxes))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% brake-drum:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0)
     (arc center outer-radius 0 360)
     (moveto P1)
     (arc center inner-radius 0 360)
     (translate center)
     (repeat screw-holes
       (bind (moveto 0 (- screw-orbit screw-radius))
             (arc 0 screw-orbit screw-radius -90 270)
             (rotate screw-angle)))))))

(define-markup-command (tamtam layout props) ()
  (let* ((width 18) (height 13) (uw 14) (r 5.25) (ch 5.5) ; width height upper-w radius center-h
  (x1 (/ (- width uw) 2)))
   (pictogram layout props width height (float-format "% tamtam:
0 0 moveto ~a ~a ~a 0 ~a ~a 3 {rlineto} repeat
~a ~a moveto ~a ~a ~a 0 360 arc"
`(,x1 ,(- height) ,uw ,x1 height ,(+ (/ width 2) r) ,ch ,(/ width 2) ,ch ,r)))))


(define-markup-command (domed-gong-a layout props) ()
  (let* ((width 12.5)
         (height 15.5)
         (inner-radius 1)
         (outer-radius 5.5)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (x-center (/ width 2))
         (P0 (cons 0 height))
         (P1 (cons width height))
         (P2 (cons x-center height))
         (P3 (cons x-center (* outer-radius 2)))
         (P4 (cons x-center (+ outer-radius inner-radius)))
         (circle-center (cons x-center outer-radius))
         (circles (list (cons circle-center outer-radius)))
         (orig-convex-hull (list P0 P1))
         (rotated-circle-bboxes
           (get-rotated-circle-bboxes rotate-img circles))
         (rotated-hull
           (append (rotate-points rotate-img orig-convex-hull)
                   rotated-circle-bboxes))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% domed-gong-a:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0) (lineto P1)
     (moveto P2) (lineto P3)
     (arc circle-center outer-radius -270 90)
     (stroke)
     (newpath)
     (moveto P4)
     (arc circle-center inner-radius -270 90)
     (gsave)
     (fill)
     (grestore)))))


;; TODO: remove unnecessary stuff?
;;       (like rotate-img here and elsewhere)
(define-markup-command (domed-gong-b layout props) ()
  (let* ((outer-radius 5.5)
         (inner-radius 1)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (width (* outer-radius 2))
         (height width)
         (center (cons outer-radius outer-radius))
         (P0 (cons width outer-radius))
         (P1 (cons (+ outer-radius inner-radius) outer-radius))

         (circles (list (cons center outer-radius)))
         (rotated-circle-bboxes
           (get-rotated-circle-bboxes rotate-img circles))
         (rotated-exts (get-bbox-extents rotated-circle-bboxes))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% domed-gong-b:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0)
     (arc center outer-radius 0 360)
     (stroke)
     (newpath)
     (moveto P1)
     (arc center inner-radius 0 360)
     (gsave)
     (fill)
     (grestore)))))


(define-markup-command (gong layout props) ()
  (let* ((width 12.5)
         (height 15.5)
         (outer-radius 5.5)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (x-center (/ width 2))
         (P0 (cons 0 height))
         (P1 (cons width height))
         (P2 (cons x-center height))
         (P3 (cons x-center (* outer-radius 2)))
         (circle-center (cons x-center outer-radius))
         (circles (list (cons circle-center outer-radius)))
         (orig-convex-hull (list P0 P1))
         (rotated-circle-bboxes
           (get-rotated-circle-bboxes rotate-img circles))
         (rotated-hull
           (append (rotate-points rotate-img orig-convex-hull)
                   rotated-circle-bboxes))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% gong:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0) (lineto P1)
     (moveto P2) (lineto P3)
     (arc circle-center outer-radius -270 90)))))


; %%%%%%%%%%%% WOODS %%%%%%%%%%%%

(define-markup-command (claves layout props) ()
  (let* ((width 12.5) (height width)
         (a 0) ; rotate-pictogram angle [0,360]
         (flip #f) ; flip-pictogram: 'x, 'y, or #f.
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         )
   (pictogram layout props (rotated-rect-w width height a) (rotated-rect-h width height a)
    (string-append
     "% claves:\n"
    ;   (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)))))

(define-markup-command (slapstick layout props) ()
  (let* ((width 12.5) (height width)
         (a 0) ; rotate-pictogram angle [0,360]
         (flip #f) ; flip-pictogram: 'x, 'y, or #f.
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         )
   (pictogram layout props (rotated-rect-w width height a) (rotated-rect-h width height a)
    (string-append
     "% slapstick:\n"
    ;   (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)))))


#!
(define-markup-command (sandpaper-blocks layout props) ()
  (let* ((width 12.5) (height width)
         (a 0) ; rotate-pictogram angle [0,360]
         (flip #f) ; flip-pictogram: 'x, 'y, or #f.
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         )
   (pictogram layout props (rotated-rect-w width height a) (rotated-rect-h width height a)
    (string-append
     "% sandpaper-blocks:\n"
    ;   (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)))))
!#
(define-markup-command (sandpaper-blocks layout props) ()
  (let* ((width 12.5)
         (height width)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% sandpaper-blocks:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)
     question-mark))))


(define-markup-command (ratchet-a layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% ratchet-a:\n" (rectstroke 0 0 width height)))))

(define-markup-command (ratchet-b layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% ratchet-b:\n" (rectstroke 0 0 width height)))))


(define-markup-command (guíro-a layout props) ()
#!
(define-markup-command (guíro-a layout props) ()
  (let* ((width 20) (height 6.25) (rw 3) (rd 2.6) (n 2)) ; ridge-width -depth number
   (pictogram layout props width height (float-format "% guíro-a:
~a ~a moveto 0 ~a 0 0 ~a 0 3 {lineto} bind repeat ~a 0 rlineto
~a {~a ~a rlineto ~a ~a rlineto} bind repeat closepath"
`(width height height width ,(/ (- width (* n rw)) 2) ,n ,(/ rw 2) ,(- rd) ,(/ rw 2) ,rd)))))
!#
  (let* ((width 12.5)
         (height width)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% guíro-a:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)
     question-mark))))


(define-markup-command (guíro-b layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% guíro-b:\n" (rectstroke 0 0 width height)))))

(define-markup-command (maraca layout props) ()
  (let* ((radius 4.5)
         (neck-height 7)
         (base-width 1.75)
         (neck-width 1.25)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (width (* radius 2))
         ;; prevent square root of a negative
         (neck-width (if (<= width neck-width)
                         width
                         neck-width))
         (height (+ neck-height
                    radius
                    (sqrt (- (expt radius 2)
                             (expt (/ neck-width 2) 2)))))
         (circle-center-y (- height radius))
         (x-center radius)
         (P0 (cons (- x-center (/ neck-width 2))
                   neck-height))
         (P1 (cons (- x-center (/ base-width 2))
                   0))
         (P2 (x-mirror P1 width))
         (P3 (x-mirror P0 width))
         (circle-center (cons radius circle-center-y))
         (circles (list (cons circle-center radius)))
         (orig-convex-hull (list P1 P2))
         (rotated-circle-bboxes
           (get-rotated-circle-bboxes rotate-img circles))
         (rotated-hull
           (append (rotate-points rotate-img orig-convex-hull)
                   rotated-circle-bboxes))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% maraca:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto width (- height radius))
     (arc circle-center radius 0 360)
     (moveto P0)
     (lineto P1 P2 P3)))))

(define-markup-command (maracas layout props) ()
  (let* ((handle-length 10)
         (radius 4)
         (a 45)
         (head-separation 2) ; handle-lngth radius angle head-separation
         (ar (* a (angle -1) 1/180))  ; deg->rad
         (hl+r (+ handle-length radius))
         (q (* hl+r (cos ar)))
         (bs (- (* 2 (- q radius)) head-separation))
         (x0 (+ q (- radius bs)))
         (width (+ (* 2 x0) bs))
         (height (+ (* hl+r (sin ar)) radius)))
   (pictogram layout props width height
     (float-format "% maracas:
~a ~a ~a ~a
2 {0 translate 0 0 moveto gsave rotate 0 ~a lineto
   0 ~a ~a -90 270 arc stroke grestore} repeat"
`(,(- 90 a) ,bs ,(- a 90) ,x0 ,hl ,hl+r ,r)))))

(define-markup-command (maracas layout props) ()
  (let* ((handle-length 10)
         (radius 4)
         (handle-angle 45)
         (head-separation 2)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (sin-handle (sin-deg handle-angle))
         (cos-handle (cos-deg handle-angle))
         (maraca-width (+ radius
                          (* (+ handle-length radius)
                             cos-handle)))
         (heads-width (+ head-separation (* radius 4)))
         (width (+ heads-width
                   (* (max 0 (- maraca-width heads-width))
                      2)))
         (height (+ radius
                    (* (+ handle-length radius)
                       sin-handle)))

         (circle-center-left-x radius)
         (circle-center-right-x (- width radius))
         (circle-center-y (- height radius))
         (circle-center-left (cons circle-center-left-x
                                   circle-center-y))
         (circle-center-right (cons circle-center-right-x
                                    circle-center-y))
         (circles (list (cons circle-center-left radius)
                        (cons circle-center-right radius)))

         (P0 (cons (* radius 2) circle-center-y))
         (P1 (cons (+ radius (* radius cos-handle))
                   (* handle-length sin-handle)))
         (P2 (cons maraca-width 0))
         (P3 (cons width circle-center-y))
         (P4 (x-mirror P1 width))
         (P5 (x-mirror P2 width))

         (orig-convex-hull (list P2 P5))
         (rotated-circle-bboxes
           (get-rotated-circle-bboxes rotate-img circles))
         (rotated-hull
           (append (rotate-points rotate-img orig-convex-hull)
                   rotated-circle-bboxes))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% maracas:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0) (arc circle-center-left radius 0 360)
     (moveto P1) (lineto P2)
     (moveto P3) (arc circle-center-right radius 0 360)
     (moveto P4) (lineto P5)))))

;; TODO: add pictograms for afuche-cabasa?
;; TODO: add bead lines to cabasa
(define-markup-command (cabasa layout props) ()
  (let* ((radius 4.5)
         (neck-height 7)
         (base-width 1.75)
         (neck-width 1.25)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (width (* radius 2))
         ;; prevent square root of a negative
         (neck-width (if (<= width neck-width)
                         width
                         neck-width))
         (height (+ neck-height
                    radius
                    (sqrt (- (expt radius 2)
                             (expt (/ neck-width 2) 2)))))
         (circle-center-y (- height radius))
         (x-center radius)
         (P0 (cons (- x-center (/ neck-width 2))
                   neck-height))
         (P1 (cons (- x-center (/ base-width 2))
                   0))
         (P2 (x-mirror P1 width))
         (P3 (x-mirror P0 width))
         (circle-center (cons radius circle-center-y))
         (circles (list (cons circle-center radius)))
         (orig-convex-hull (list P1 P2))
         (rotated-circle-bboxes
           (get-rotated-circle-bboxes rotate-img circles))
         (rotated-hull
           (append (rotate-points rotate-img orig-convex-hull)
                   rotated-circle-bboxes))
         (rotated-exts (get-bbox-extents rotated-hull))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% cabasa:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto width (- height radius))
     (arc circle-center radius 0 360)
     (moveto P0)
     (lineto P1 P2 P3)))))

(define-markup-command (woodblock layout props) ()
  (let* ((width 12.5)
         (height width)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% woodblock:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)
     question-mark))))

(define-markup-command (temple-block layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% temple-block:\n" (rectstroke 0 0 width height)))))


(define-markup-command (slit-drum layout props) ()
  (let* ((width 12.5)
         (height width)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% slit-drum:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)
     question-mark))))

(define-markup-command (log-drum layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% log-drum:\n" (rectstroke 0 0 width height)))))


; %%%%%%%%%%% PITCHED %%%%%%%%%%%

(define-markup-command (glockenspiel layout props) ()
  (pictogram layout props 20 15
"% glockenspiel:
20 15 7.5 % width height rh
0 0 moveto 0 2 index rlineto
dup 3 -1 roll sub 2 div 3 -1 roll exch rlineto
neg 0 exch rlineto closepath"))

(define-markup-command (glockenspiel layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% glockenspiel:\n" (rectstroke 0 0 width height)))))

(define-markup-command (celesta layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% celesta:\n" (rectstroke 0 0 width height)))))

(define-markup-command (xylophone layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% xylophone:\n" (rectstroke 0 0 width height)))))

(define-markup-command (vibraphone layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% vibraphone:\n" (rectstroke 0 0 width height)))))

(define-markup-command (marimba layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% marimba:\n" (rectstroke 0 0 width height)))))

; % linear
(define-markup-command (chimes-a layout props) ()
  (let* ((cw 3) (maxh 15) (minh 10) (sl 3) (edge-sep 2) (sep 5) (n 3)
         (open? #t)
         (width (+ (* edge-sep 2) (* sep (- n 1)))) (height (+ maxh sl)))
   (pictogram layout props width height (float-format "% chimes-a:
0 ~a moveto ~a 0 rlineto stroke
0 1 ~a {
  gsave
  dup ~a mul ~a add ~a translate
  0 0 moveto 0 ~a rlineto
  ~a div ~a mul ~a sub ~a 1 index rmoveto
  0 exch ~a 0 0 3 index neg 3 {rlineto} repeat~a stroke
  grestore} bind for"
`(height width
,(- n 1)
,sep ,edge-sep height
,(- sl)
,(- n 1) ,(- maxh minh) ,maxh ,(/ cw -2)
,cw ,(if open? "" "\nclosepath"))))))

#!
%{
0 ~a moveto ~a 0 rlineto stroke
0 1 ~a {
  gsave                      % x
  dup                        % x x
  ~a mul ~a add ~a translate % x % (sep) (edge-sep) (height)
  0 0 moveto 0 ~a rlineto    % x % (-sl)
  ~a div ~a mul ~a sub       % y % (n-1) (max-min) (max)
  ~a 1 index rmoveto         % y % -cw/2
  0 exch ~a 0 0 3 index neg  %   % (cw)
  3 {rlineto} repeat~a
  stroke
  grestore} bind for

% parabolic
(define-markup-command (chimes-b layout props) ()
  (let* ((maxh 12) (minh 7) (afh 2) (arh 2) (sep 3) (n 5)
         (width (* sep (+ n 1))) (height (+ maxh afh)))
   (pictogram layout props width height (float-format "% chimes-b:
0 0 moveto 0 ~a ~a 0 0 ~a 3 {rlineto} repeat
0 1 ~a {dup 1 add ~a mul ~a moveto 0 exch
       ~a sub ~a div dup mul ~a mul ~a sub rlineto} for"
`(,(- arh height) width ,(- height arh)
,(- n 1) ,sep height
,(- n 1) ,(- n 1) ,(- minh maxh) ,minh)))))

%}
!#

; % linear
(define-markup-command (chimes-b layout props) ()
  (let* ((maxh 20) (minh 16)
         (afh 2) (arh 1.5) (sep 2) (n 4) (dh 8)
         (width (* sep (+ n 1))) (height (+ maxh afh)))
   (pictogram layout props width height (float-format "% chimes-b:
0 0 moveto 0 ~a ~a 0 0 ~a 3 {rlineto} repeat
0 ~a moveto ~a 0 rlineto
1 1 ~a {dup ~a mul ~a moveto 0 exch ~a div ~a mul ~a sub rlineto} bind for"
`(,(- arh height) width ,(- height arh)
,dh width
,n ,sep height ,n ,(- maxh minh) ,maxh)))))

; %%%%%%%%%% MEMBRANES %%%%%%%%%%

(define-markup-command (tambourine layout props) ()
  (let* ((r1 2) (r2 8) (jingles 4) (width (* (+ r1 r2) 2)) (cntr (/ width 2)))
   (pictogram layout props width width (float-format "% tambourine:
~a ~a moveto ~a ~a ~a 0 360 arc
~a ~a translate ~a {0 ~a moveto 0 ~a ~a -90 270 arc ~a rotate} bind repeat"
`(,(- width r1) ,(+ r1 r2) ,cntr ,cntr ,r2
,cntr ,cntr ,jingles ,(- r2 r1) ,r2 ,r1 ,(/ 360 jingles))))))

(define-markup-command (bongos layout props) ()
  (let* ((hw1 4.75) (bw1 2)   (h1 4.5)
         (hw2 8.25) (bw2 4.5) (h2 6)  (sep 2.5)
         (bh 3) (bt 0.5) ; beam: -height -thickness
         (dx1 (/ (- hw1 bw1) 2)) (dx2 (/ (- hw2 bw2) 2))
         (width (+ hw1 sep hw2)) (height (max h1 h2)))
   (pictogram layout props width height (float-format "% bongos:
{moveto 3 {rlineto} repeat} bind
~a ~a ~a 0 ~a ~a ~a ~a 8 index exec
~a ~a ~a 0 ~a ~a ~a ~a 8 index exec stroke
~a ~a ~a 0 ~a ~a ~a ~a 8 index exec clip
newpath 0 ~a ~a 0 0 ~a ~a ~a 9 -1 roll exec closepath
gsave fill grestore"
`(,(- dx2) ,(- h2) ,hw2 ,(- dx2) ,h2 ,(+ hw1 sep dx2) ,(- height h2)
 ,(- dx1) ,(- h1) ,hw1 ,(- dx1) ,h1 ,dx1 ,(- height h1)
 ,dx2 ,(- h2) ,sep ,dx1 ,h1 ,(+ dx1 bw1) ,(- height h1)
 ,(- bt) ,(+ dx1 sep dx2) ,bt ,(+ dx1 bw1) ,(- bh (/ bt 2)))))))

(define-markup-command (timbales layout props) ()
  (let* ((w1 8.25) (h1 10) (w2 5) (h2 5) (sep 4.5)
         (bh 6.75) (bt 0.5) ; beam: -height -thickness
         (width (+ w1 sep w2)) (height (max h1 h2)))
   (pictogram layout props width height (float-format "% timbales:
{moveto 3 {rlineto} repeat} bind
0 ~a ~a 0 0 ~a 0 ~a 8 index exec
0 ~a ~a 0 0 ~a ~a ~a 8 index exec stroke
0 ~a ~a 0 0 ~a ~a ~a 9 -1 roll exec closepath
gsave fill grestore"
`(,(- h1) ,w1 ,h1 ,(- height h1)
  ,(- h2) ,w2 ,h2 ,(+ w1 sep) ,(- height h2)
  ,(- bt) ,sep ,bt ,w1 ,(- bh (/ bt 2)))))))

(define-markup-command (conga layout props) ()
  (let* ((width 9.25) (height 17)
         (hw 6) (hh 1.25)    ; head-w head-h
         (bw 4.25) (hwp 10)  ; base-w h@widest-pt
         (ub 0.5) (ucf 0.75)  ; upper-breadth upper-curve-factor
         (lb 0.25) (lcf 0.75) ; lower-breadth lower-curve-factor
         (rotate-img 0) (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (P0 (cons (/ (- width bw) 2) 0))
         (V0 (cons 0 (* hwp (- 1 lb))))
         (P1 (cons 0 hwp))
         (V1 (cons 0 (+ hwp (* (- height hwp) ub))))
         (P2 (cons (/ (- width hw) 2) height))
         (P3 (x-mirror P2 width))
         (V2 (x-mirror V1 width))
         (P4 (x-mirror P1 width))
         (V3 (x-mirror V0 width))
         (P5 (x-mirror P0 width))
         ;; paranoid and overkill; but the reasoning is sound:
         (M01 (vertex-curveto-midpoint P0 V0 P1 lcf))
         (M12 (vertex-curveto-midpoint P1 V1 P2 ucf))
         (M34 (vertex-curveto-midpoint P3 V2 P4 ucf))
         (M45 (vertex-curveto-midpoint P4 V3 P5 lcf))
         (orig-convex-hull (list P0 M01 P1 M12 P2 P3 M34 P4 M45 P5))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% conga:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (moveto P0)
     (vertex-curveto P0 V0 P1 lcf)
     (vertex-curveto P1 V1 P2 ucf)
     (lineto P3)
     (vertex-curveto P3 V2 P4 ucf)
     (vertex-curveto P4 V3 P5 lcf)
     "closepath gsave stroke grestore clip\n"
     (moveto 0 (- height hh))
     (lineto width (- height hh))))))


;; TODO: create a (non-diagonal) make-squiggles procedure?
(define
  (make-diagonal-squiggles
    left-endpoint
    right-endpoint
    squiggles
    curve-factor
    reflect?)
  (let* ((dx (- (car right-endpoint) (car left-endpoint)))
         (dy (- (cdr right-endpoint) (cdr left-endpoint)))
         (width (abs dx)) ; dx should be positive anyway
         (height (abs dy))
         (tall-and-skinny? (< width height))
         (top-to-bottom? (negative? dy))

         ;; * if curve-factor is set to 0, this block resets it
         ;;   automatically based on width and height
         ;; * squares use "default-curve-factor"
         ;; * more oblong rectangles use a higher value to
         ;;   exaggerate the curve since the "amplitude" of the
         ;;   squiggle is limited by the rectangle's sides
         ;; * non-zero values override this block
         (curve-factor
           (if (zero? curve-factor)
               (- 1 (* (- 1 default-curve-factor)
                       (/ (min width height)
                          (max width height))))
               curve-factor))

         (overall-angle (atan-deg (/ width height)))
         (P0->P3-distance (/ (sqrt (+ (* width width)
                                      (* height height)))
                             (* 2 squiggles)))
         (P0->vertex-distance
           (/ (/ P0->P3-distance 2)
              ((if tall-and-skinny? cos-deg sin-deg)
               overall-angle)))

         ; these are relative distances (from start of rcurveto)
         (P3-x (/ dx 2 squiggles))
         (P3-y (/ dy 2 squiggles))

         ;; with 1 or 2 squiggles, curve starts in the direction
         ;; of the shorter side.  With 3 or more squiggles, curve
         ;; starts in the direction of the longer side.  It
         ;; usually looks nicer this way, and can be reversed by
         ;; setting REFLECT? to #t.
         (vertex-x
           (if tall-and-skinny?
               (if reflect?
                   (if (< 2 squiggles)
                       P3-x
                       0)
                   (if (< 2 squiggles)
                       0
                       P3-x))
               (if reflect?
                   (if (< 2 squiggles)
                       (- P3-x P0->vertex-distance)
                       P0->vertex-distance)
                   (if (< 2 squiggles)
                       P0->vertex-distance
                       (- P3-x P0->vertex-distance)))))
         (vertex-y
           (if tall-and-skinny?
               (if reflect?
                   (if (< 2 squiggles)
                       ((if top-to-bottom? + -)
                        P3-y
                        P0->vertex-distance)
                       ((if top-to-bottom? - +)
                        P0->vertex-distance))
                   (if (< 2 squiggles)
                       ((if top-to-bottom? - +)
                        P0->vertex-distance)
                       ((if top-to-bottom? + -)
                        P3-y
                        P0->vertex-distance)))
               (if reflect?
                   (if (< 2 squiggles) P3-y 0)
                   (if (< 2 squiggles) 0 P3-y))))

         (vertex-P3-dx (- P3-x vertex-x))
         (vertex-P3-dy (- P3-y vertex-y))

         (P1-x (* vertex-x curve-factor))
         (P1-y (* vertex-y curve-factor))
         (P2-x (- P3-x (* vertex-P3-dx curve-factor)))
         (P2-y (- P3-y (* vertex-P3-dy curve-factor)))

         ;; relative to left-endpoint
         (P1 (cons P1-x P1-y))
         (P2 (cons P2-x P2-y))
         (P3 (cons P3-x P3-y))

         ;; relative to P3
         (P4 (cons (- (car P3) (car P2))
                   (- (cdr P3) (cdr P2))))
         (P5 (cons (- (car P3) (car P1))
                   (- (cdr P3) (cdr P1))))
         (P6 P3))
   (string-append
     (moveto left-endpoint)
     (repeat squiggles (bind (rcurveto P1 P2 P3)
                             (rcurveto P4 P5 P6))))))


(define-markup-command (snare-a layout props) ()
  (let* ((width 17.9)
         (height 6.25)
         (squiggles 4)

         ;; if this is 0, the (make-squiggles) procedure will set
         ;; a value automatically (based on width and height).
         ;; Change it to override the automatic value:
         (curve-factor 0)

         (reflect-squiggle? #f)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% snare-a:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)
     (make-diagonal-squiggles
       (cons 0 height)
       (cons width 0)
       squiggles
       curve-factor
       reflect-squiggle?)))))

(define-markup-command (snare-b layout props) ()
  (let* ((dw 17.9) (height 6.25) (sep 2.5) ; drum-w height separation
         (al 5.75) (hw 3) ; arrow-l head-w
         (hh 2.75) (ht 1.375) ; head-h head-thick
         (hw/2 (/ hw 2)) (ah (- height (/ (- height al) 2))))
   (pictogram layout props (+ dw sep hw) height
    (string-append
     "% snare-b:\n"
     (rectstroke 0 0 dw height)
     (moveto (+ dw sep hw/2) ah)
     (rlineto 0 (- al))
     (stroke)
     (moveto (+ dw sep) (- ah hh))

     ;; more concise ps-code, less readable here:
     ; (objects->ps-string (- hw/2) (- hh ht) hw/2 (- hh) hw/2 hh)
     ; (repeat 3 "rlineto")

     ;; more readable here:
     (rlineto hw/2 hh hw/2 (- hh) (- hw/2) (- hh ht))

     (closepath)
     (gsave)
     (fill)
     (grestore)))))


(define-markup-command (snare-off-a layout props) ()
  (let* ((width 17.9)
         (height 6.25)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% snare-off-a:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)))))


(define-markup-command (snare-off-b layout props) ()
  (let* ((dw 17.9) (height 6.25) (sep 2.5) ; drum-w height separation
         (al 5.75) (hw 3) (hh 2.75) (ht 1.375) ; arrow-l head-w head-h head-thick
         (hw/2 (/ hw 2)) (ah (- height (/ (- height al) 2))))
   (pictogram layout props (+ dw sep hw) height (float-format "% snare-off-b:
0 0 ~a ~a rectstroke
~a ~a moveto 0 ~a rlineto stroke
~a ~a moveto ~a ~a ~a ~a ~a ~a 3 {rlineto} repeat closepath
gsave fill grestore"
`(,dw height
,(+ dw sep hw/2) ,ah ,(- al)
,(+ dw sep) ,(+ hh (- height ah)) ,(- hw/2) ,(- ht hh) ,hw/2 ,hh ,hw/2 ,(- hh))))))

(define-markup-command (military-drum layout props) ()
  (let* ((width 6.5)
         (height 15.625)
         (squiggles 2)

         ;; if this is 0, the (make-squiggles) procedure will set
         ;; a value automatically (based on width and height).
         ;; Change it to override the automatic value:
         (curve-factor 0)

         (reflect-squiggle? #f)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% military-drum:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)
     (make-diagonal-squiggles
       (cons 0 height)
       (cons width 0)
       squiggles
       curve-factor
       reflect-squiggle?)))))


(define-markup-command (tenor-drum layout props) ()
  (let* ((width 6.5)
         (height 15.625)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% tenor-drum:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)))))


(define-markup-command (tomtom layout props) ()
  (let* ((width 11.2)
         (height width)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% tomtom:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)))))


(define-markup-command (tomtom-wood-top layout props) ()
  (let* ((width 11.2)
         (height width)
         (top-height 2)
         (rotate-img 0)
         (flip-img #f) ; 'x 'y or #f
         ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         (orig-convex-hull (rect-hull width height))
         (rotated-exts
           (get-rotated-exts orig-convex-hull rotate-img))
         (rotated-dims (exts->dims rotated-exts)))
   (pictogram layout props (car rotated-dims) (cdr rotated-dims)
    (string-append
     "% tomtom-wood-top:\n"
     (rotate-and-flip-image rotate-img flip-img rotated-exts)
     (rectstroke 0 0 width height)
     (moveto 0 (- height top-height))
     (rlineto width 0)))))

(define-markup-command (bass-drum-a layout props) ()
  (let* ((r 6.75) (sh 7.25) (va 17) ; radius side-h viewing-angle (0<va<90)
         (var (* va (angle -1) 1/180))
         (ash (* sh (cos var))) (ar (* r (sin var)))
         (width (+ ash (* 2 ar))) (height (* 2 r)) (dx (* 4/3 ar)))
   (pictogram layout props width height (float-format "% bass-drum-a:
~a 0 ~a ~a 2 {moveto ~a 0 rlineto} repeat
~a 0 ~a ~a 0 ~a rcurveto
~a 0 ~a 0 2 {moveto ~a 0 ~a ~a 0 ~a rcurveto} repeat"
`(,ar ,ar height ,ash
,dx ,dx height height
,ar ,(+ ar ash) ,(- dx) ,(- dx) height height)))))

(define-markup-command (bass-drum-a layout props) ()
  (let* ((r 6.75) (sh 7.25) (va 17) ; radius side-h viewing-angle (0<va<90)
         (var (* va (angle -1) 1/180))
         (ash (* sh (cos var))) (ar (* r (sin var)))
         (width (+ ash (* 2 ar))) (height (* 2 r)) (dx (* 4/3 ar)))
   (pictogram layout props width height (ly:format "% bass-drum-a:
~a 0 ~a ~a 2 {moveto ~a 0 rlineto} repeat
~a 0 ~a ~a 0 ~a rcurveto
~a 0 ~a 0 2 {moveto ~a 0 ~a ~a 0 ~a rcurveto} repeat"
ar ar height ash
dx dx height height
ar (+ ar ash) (- dx) (- dx) height height))))

(define-markup-command (bass-drum-b layout props) ()
  (let* ((r 6.75) (sh 7.25) (va 17) ; radius side-h viewing-angle (0<va<90)
         (var (* va (angle -1) 1/180))
         (ash (* sh (cos var))) (ar (* r (sin var)))
         (width (* 2 r)) (height (+ ash (* 2 ar))) (dy (* 4/3 ar)))
   (pictogram layout props width height (float-format "% bass-drum-b:
0 ~a 2 {~a moveto 0 ~a rlineto} repeat
0 ~a ~a ~a ~a 0 rcurveto
0 ~a 0 ~a 2 {moveto 0 ~a ~a ~a ~a 0 rcurveto} repeat"
`(width ,ar ,ash
,dy width ,dy width
,ar ,(+ ar ash) ,(- dy) width ,(- dy) width)))))


; %%%%%%%%%%% EFFECTS %%%%%%%%%%%

(define-markup-command (police-whistle layout props) ()
  (let* ((r 3.75) ; radius
         (mt 1.375) ; mouthpiece-thickness
         (ml 3.8) ; mouthpiece-length
         (flip-x? #f) ; flip-horizontally?
         (width (+ ml r (sqrt (+ (expt r 2) (expt (- r mt) 2)))))
         (height (* 2 r))
         (a (* (asin (/ (- r mt) r)) 180 (/ (angle -1)))))
   (pictogram layout props width height
    (float-format "% police-whistle (width height -> ~a ~a):
~a ~a moveto ~a ~a ~a ~a 90 arc~a ~a 0 rlineto closepath"
(if flip-x?
 `(width height width ,(- height mt) ,r       ,r ,r ,a          "n" ,(- width r))
 `(width height  0 ,(- height mt) ,(- width r) ,r ,r ,(- -180 a) ""  ,(- r width)))))))

(define-markup-command (slide-whistle-a layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% slide-whistle-a:\n" (rectstroke 0 0 width height)))))

(define-markup-command (slide-whistle-b layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% slide-whistle-b:\n" (rectstroke 0 0 width height)))))

(define-markup-command (bird-whistle layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% bird-whistle:\n" (rectstroke 0 0 width height)))))

(define-markup-command (duck-call layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% duck-call:\n" (rectstroke 0 0 width height)))))

(define-markup-command (wind-whistle layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% wind-whistle:\n" (rectstroke 0 0 width height)))))

(define-markup-command (siren layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% siren:\n" (rectstroke 0 0 width height)))))

(define-markup-command (klaxon-horn layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% klaxon-horn:\n" (rectstroke 0 0 width height)))))

(define-markup-command (auto-horn layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% auto-horn:\n" (rectstroke 0 0 width height)))))

(define-markup-command (gun layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% gun:\n" (rectstroke 0 0 width height)))))

(define-markup-command (lions-roar layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% lions-roar:\n" (rectstroke 0 0 width height)))))

(define-markup-command (wind-machine layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% wind-machine:\n" (rectstroke 0 0 width height)))))

(define-markup-command (thunder-sheet layout props) ()
  (let* ((width 18) (height 13) (uw 14) (sw 10) (sh 8) ; width height upper-w sheet-w sheet-h
         (ss 5) (sl 3) ; string-separation string-length
         (x1 (/ (- width uw) 2)) (x2 (/ (- width sw) 2)))
   (pictogram layout props width height (float-format "% thunder-sheet:
0 0 moveto ~a ~a ~a 0 ~a ~a 3 {rlineto} repeat
0 ~a ~a 0 0 ~a ~a ~a 2 {rmoveto rlineto} repeat
~a ~a ~a ~a rectstroke"
`(,x1 ,(- height) ,uw ,x1 height
,sl ,ss ,(- sl) ,(/ (+ width ss) -2) height
,x2 ,(- height sl sh) ,sw ,sh)))))

(define-markup-command (anvil layout props) () (pictogram layout props 22 14
"% anvil:
22 10 5 8 5           % width height base-x0 base-w waist-w
2 copy sub 2 div      % width height bx0 bw ww base-waist-dx
3 index add           % width height bx0 bw ww bx1
dup 3 -1 roll add     % width height bx0 bw bx1 bx2
3 -1 roll 3 index add % width height bx0 bx1 bx2 bx3
                      % better: width h/2 bx3 bx2 bx1 bx0
4 -1 roll 0 moveto    % width height bx1 bx2 bx3
4 -1 roll 2 div dup   % width bx1 bx2 bx3 h/2 h/2
5 -1 roll exch lineto % width bx2 bx3 h/2
dup 0 exch lineto     % width bx2 bx3 h/2
dup 0 exch rlineto    % width bx2 bx3 h/2
4 -1 roll 0 rlineto   % bx2 bx3 h/2
3 -1 roll exch lineto % bx3
0 lineto closepath"))

#!
%{
(define-markup-command (anvil layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height (float-format "% anvil:
0 0 ~a ~a rectstroke"
`(width height)))))
%}
!#

; %%%%%%%%%%% TIMPANI %%%%%%%%%%%


; %%% STICKS, MALLETS, BEATERS %%%

(define dmw  7.5) ; %  7.5 default mallet width
(define dmh 37.5) ; % 37.5 default mallet height

(define-markup-command (snare-stick-a layout props) ()
  (let* ((width 3) (height 30))
   (pictogram layout props width height (float-format "% snare-stick-a:
0 0 moveto ~a 0 lineto ~a ~a lineto closepath"
`(width ,(/ width 2) height)))))

(define-markup-command (snare-stick-b layout props) ()
  (let* ((width 2.25) (nw 1.25) (tw 2.25) (hl 28) (nl 4.5) (tl 3.5)
         (height (+ (/ width 2) hl nl tl))
         (bezw (/ (- tw nw) 2)) (bezh (/ tl 2))
         (bezh2 (/ (sqrt (+ (* bezw bezw) (* bezh bezh)))
                   4 (cos (atan (/ bezw bezh)))))
         (bezh1 (- bezh bezh2))
         (k 0.5523) (x1 (* k bezw))
         (y1 (* k bezh1)) (y2 (- bezh (* k bezh2))) (y3 (* 2/3 tl)))
   (pictogram layout props width height (float-format "% snare-stick-b:
~a ~a moveto ~a ~a rlineto
~a ~a ~a ~a ~a ~a rcurveto
0 ~a ~a ~a ~a 0 rcurveto
0 ~a ~a ~a ~a ~a rcurveto
~a ~a rlineto 0 ~a rlineto
~a ~a ~a 180 360 arc closepath"
`(width ,(- height tl nl) ,(/ (- nw width) 2) ,nl
  ,x1 ,y1 ,bezw ,y2 ,bezw ,bezh
  ,y3 ,(- tw) ,y3 ,(- tw)
  ,(- y2 bezh) ,(- bezw x1) ,(- y1 bezh) ,bezw ,(- bezh)
  ,(/ (- nw width) 2) ,(- nl) ,(- hl)
  ,(/ width 2) ,(/ width 2) ,(/ width 2))))))

; % with round tip
(define-markup-command (plastic-snare-stick layout props) ()
  (let* ((width 3) (height 30) (r 1))
   (pictogram layout props width height (float-format "% plastic-snare-stick:
~a 0 moveto ~a ~a ~a -90 270 arc
gsave fill grestore
0 0 lineto closepath"
`(width ,(/ width 2) ,(- height r) ,r width)))))

; % with square tip
(define-markup-command (plastic-snare-stick layout props) ()
  (let* ((width 3) (height 30) (tw 2.5)) ; tip-width
   (pictogram layout props width height (float-format "% plastic-snare-stick:
0 0 moveto ~a ~a lineto ~a 0 lineto closepath
~a ~a ~a ~a rectfill"
`(,(/ width 2) ,(- height tw) width
,(/ (- width tw) 2) ,(- height tw) ,tw ,tw)))))

(define-markup-command (spoon-mallet layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% spoon-mallet:\n" (rectstroke 0 0 width height)))))

(define-markup-command (spoon-mallet layout props) ()
  (let* ((width 5) (hl 20) (a 45) ; width handle-length angle
         (ar (* a (angle -1) 1/180)) ; deg->rad
         (r (/ width 2)) (dy (/ r (sin (/ ar 2))))
  )
   (pictogram layout props width (+ hl dy r) (float-format "% spoon-mallet:
~a ~a moveto ~a ~a ~a ~a ~a arc closepath ~a 0 lineto"
`(,r ,hl ,r ,(+ hl dy) ,r ,(/ a -2) ,(+ 180 (/ a 2)) ,r)))))

(define-markup-command (wire-brush layout props) ()
  (let* ((hl 27) (wl 10) ; handle-length wire-length
         (a 45) (n 4) ; angle num-of-wires
         (ar (* a (angle -1) 1/180)) ; deg->rad
         (width (* 2 wl (sin (/ ar 2))))
         (height (+ (* wl (cos (* 1/2 (/ ar (- n 1)) (remainder (- n 1) 2)))) hl)))
   (pictogram layout props width height (float-format "% wire-brush:
~a 0 moveto 0 ~a rlineto ~a rotate
~a {0 ~a rlineto 0 ~a rmoveto ~a rotate} bind repeat"
`(,(/ width 2) ,hl ,(/ a -2)
,n ,wl ,(- wl) ,(/ a (- n 1)))))))

(define-markup-command (triangle-beater layout props) ()
  (let* ((width dmw) (height dmh) (x-center (/ width 2))
         (th (* width (sqrt 3) 1/2)) (y1 (- (/ height 2) (/ x-center (sqrt 3)))))
   (pictogram layout props width height (float-format "% triangle-beater:
0 ~a moveto ~a ~a ~a ~a 2 {rlineto} repeat closepath
~a 0 moveto 0 ~a rlineto"
`(,y1 ,xc ,(- th) ,xc ,th ,xc height)))))

(define-markup-command (knitting-needle layout props) ()
  (let* ((width dmw) (height dmh))
   (pictogram layout props width height (float-format "% knitting-needle:
~a 0 0 0 ~a ~a 2 {0 moveto rlineto} repeat"
`(width height ,(/ width 2))))))

(define-markup-command (guíro-scraper layout props) ()
  (let* ((width 8) (height 30) (tine-l 15))
   (pictogram layout props width height (float-format "% guíro-scraper:
0 ~a moveto 0 ~a ~a 0 0 ~a 3 {rlineto} repeat
~a 0 moveto 0 ~a rlineto"
`(height ,tine-l width ,(- tine-l) ,(/ width 2) height)))))

(define-markup-command (bow layout props) () (pictogram layout props 2 dmh
(ly:format "% bow:
2 ~a 4 3 1 % width height tip-h tail-h flip (1 or -1)
dup -1 eq {4 index 0 translate} if 5 -1 roll mul 4 1 roll
3 index 1 index moveto 0 exch lineto exch 0 exch lineto
1 index exch neg rlineto 0 lineto" dmh)))

#!
%{
(define-markup-command (bow layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height (float-format "% bow:
0 0 ~a ~a rectstroke"
`(width height)))))
%}
!#

(define-markup-command (fingernail layout props) ()
  (let* ((width 12.5) (height 5) (length 3)
         (j 1/4) (k (- 1 j)) ; not sure about this...
         (x1 (* width j)) (x2 (* width k))
         (dy1 (* (- length height) 4/3)) (dy2 (* height -4/3)) )
   (pictogram layout props width height (float-format "% fingernail:
0 ~a moveto ~a ~a ~a ~a ~a 0 rcurveto
~a ~a ~a ~a ~a 0 rcurveto"
`(height ,x1 ,dy1 ,x2 ,dy1 width ,(- x1) ,dy2 ,(- x2) ,dy2 ,(- width))))))

(define-markup-command (hand layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% hand:\n" (rectstroke 0 0 width height)))))

(define-markup-command (fist layout props) ()
  (let* ((width 12.5) (height width))
   (pictogram layout props width height
     (string-append "% fist:\n" (rectstroke 0 0 width height)))))

(define-markup-command (coin-a layout props) ()
  (let* ((r 5) (sr 1.5))
   (pictogram layout props (* 2 r) (* 2 r) (float-format "% coin-b:
~a 0 moveto ~a ~a ~a -90 270 arc
~a ~a moveto ~a ~a ~a 180 30 arcn
~a ~a moveto ~a ~a ~a 180 330 arc
~a ~a moveto 0 ~a rlineto"
`(,r ,r ,r ,r
,(- r sr) ,r ,r ,r ,sr
,(- r sr) ,r ,r ,r ,sr
,r ,(- r (* 1.875 sr)) ,(* 3.75 sr))))))

(define-markup-command (coin-b layout props) ()
  (let* ((r 5.75) (sr 1.5))
   (pictogram layout props (* 2 r) (* 2 r) (float-format "% coin-b:
~a 0 moveto ~a ~a ~a -90 270 arc
~a ~a moveto ~a ~a ~a 270 0 arcn
~a ~a moveto ~a ~a ~a 90 -180 arcn
~a ~a moveto 0 ~a rlineto"
`(,r ,r ,r ,r
,r ,r ,r ,(+ r sr) ,sr
,r ,r ,r ,(- r sr) ,sr
,r ,(- r (* 2.5 sr)) ,(* 5 sr))))))

(define-markup-command (coin-c layout props) ()
  (let* ((r 5.75) (scl 0.5) ; proportions correct at scl=1
         (euro-linewidth 0.875) ; kludge
         (width (* 2 r)) (height width)
         (er (* 5.5 scl)) (ew (* 11.3361 scl))
         (x1 (- r (* 7.2922 scl))) (x2 (+ (* 7.2922 scl) x1))
         (dx1 (+ (* 2.0785 scl) (- x2 x1))) (dx2 (+ (* 2.9099 scl) (- x2 x1)))
         (x5 (+ x1 ew)) (y1 (- r scl)) (y2 (+ r scl)) (y3 (+ (* 3.7279 scl) r))
         (a1 42.6717) (a2 314.1392))
   (pictogram layout props width height (float-format "% coin-c:
~a 0 moveto ~a ~a ~a -90 270 arc stroke
currentlinewidth dup ~a mul setlinewidth
~a ~a moveto ~a ~a ~a ~a ~a arc
~a ~a ~a ~a ~a ~a 2 {moveto 0 rlineto} repeat stroke
setlinewidth"
`(,r ,r ,r ,r
,euro-linewidth
,x5 ,y3 ,x2 ,r ,er ,a1 ,a2
,dx2 ,x1 ,y2 ,dx1 ,x1 ,y1)))))

(define-markup-command (chime-hammer layout props) ()
  (let* ((width 12.5) (height 25) (hh 5) (hw 2)) ; width height head-h handle-w
   (pictogram layout props width height (float-format "% chime-hammer:
0 ~a ~a ~a rectstroke ~a 0 ~a ~a rectstroke"
`(,(- height hh) width ,hh ,(/ (- width hw) 2) ,hw ,(- height hh))))))

(define-markup-command (metal-hammer layout props) ()
  (let* ((width 12.5) (height 25) (uw 9) (hh 4) ; upper-w head-h
         (hw 2) (hcx 5)) ; handle-w handle-center-x
   (pictogram layout props width height (float-format "% metal-hammer:
~a ~a moveto ~a 0 0 ~a ~a 0 3 {rlineto} repeat closepath
~a 0 ~a ~a rectstroke"
`(,uw height width ,(- hh) ,(- uw)
,(/ (- width hw) 2) ,hw ,(- height hh))))))


(define-markup-command (soft-mallet        layout props) () (mallet-pictogram layout props 0 0))
(define-markup-command (med-mallet         layout props) () (mallet-pictogram layout props 0 1))
(define-markup-command (hard-mallet        layout props) () (mallet-pictogram layout props 0 2))
(define-markup-command (brass-mallet       layout props) () (mallet-pictogram layout props 0 3))
(define-markup-command (soft-yarn-mallet   layout props) () (mallet-pictogram layout props 1 0))
(define-markup-command (med-yarn-mallet    layout props) () (mallet-pictogram layout props 1 1))
(define-markup-command (hard-yarn-mallet   layout props) () (mallet-pictogram layout props 1 2))
(define-markup-command (soft-timp-mallet-a layout props) () (mallet-pictogram layout props 2 0))
(define-markup-command (med-timp-mallet-a  layout props) () (mallet-pictogram layout props 2 1))
(define-markup-command (hard-timp-mallet-a layout props) () (mallet-pictogram layout props 2 2))
(define-markup-command (wood-timp-mallet-a layout props) () (mallet-pictogram layout props 2 3))
(define-markup-command (soft-timp-mallet-b layout props) () (mallet-pictogram layout props 3 0))
(define-markup-command (med-timp-mallet-b  layout props) () (mallet-pictogram layout props 3 1))
(define-markup-command (hard-timp-mallet-b layout props) () (mallet-pictogram layout props 3 2))
(define-markup-command (wood-timp-mallet-b layout props) () (mallet-pictogram layout props 3 3))
(define-markup-command (soft-bass-mallet   layout props) () (mallet-pictogram layout props 4 0))
(define-markup-command (med-bass-mallet    layout props) () (mallet-pictogram layout props 4 1))
(define-markup-command (hard-bass-mallet   layout props) () (mallet-pictogram layout props 4 2))
(define-markup-command
                     (steel-core-tam-mallet layout props) () (mallet-pictogram layout props 4 3))
(define-markup-command (two-head-mallet    layout props) () (mallet-pictogram layout props 5 0))


(define-markup-command (guiro-a layout props) ()
  (interpret-markup layout props (markup #:guíro-a)))

(define-markup-command (guiro-b layout props) ()
  (interpret-markup layout props (markup #:guíro-b)))

(define-markup-command (antique-cymbal layout props) ()
  (interpret-markup layout props (markup #:crotale)))

(define-markup-command (antique-cymbals layout props) ()
  (interpret-markup layout props (markup #:crotales)))

(define-markup-command (almglocke layout props) ()
  (interpret-markup layout props (markup #:herd-bell)))


(define-markup-command (format-stencil layout props name stencil)
  (string? markup?)
  (interpret-markup layout props
         (markup
           #:pad-to-box '(0 . 15) '(-0.5 . 2.5) #:typewriter #:fontsize -3 name
           #:pad-to-box '(-2 . 10) '(-0.5 . 2.5) stencil)))

(define-markup-command (format-stencil-b layout props name stencil)
  (string? markup?)
  (interpret-markup layout props
         (markup
           #:pad-to-box '(0 . 20) '(-0.5 . 2.5) #:typewriter #:fontsize -3 name
           #:pad-to-box '(-2 . 10) '(-0.5 . 2.5) stencil)))

;(display (pairaverage '(0 . 0) '(1 . 1)))
;(display (a-b-slope '(0 . 1) '(0 . 2)))
;(display (rotate-points 90 '(0 . 0) '(1 . 0) '(0 . 1)))
;(display (get-precision 123.450))
;(display (a-b-slope '(0 . 0) '(-1 . -1)))

;(display (line-intersection '((0 . 0) . 90) '((1 . 1) . 180)))
;(display (Z-curveto '(0 . 1) '(1 . 0) 0 0.75 0.75))
;(display (U-curveto '(0 . 0) '(1 . 0) 1))
;(display (L-curveto '(0 . 1) '(1 . 0) -90 0.75 0.75))
;(display (V-curveto '(0 . 0) '(1 . 1) 90 -60 0.75 0.75))
;(display (rotate-point 90 '(1 . 0)))
;(display (rotate-points 90 '((1 . 0) (0 . 1))))
;(display (objects->ps-string 0 `(,(sqrt 2) . 2) pi 'dup))
;(display (3points->circle '(0 . 0) '(1 . 0) '(1 . 0)))
;(display (arch-curveto '(0 . 0) '(1 . 0) 1 'angle 67.5))
;(display (arch-curveto '(0 . 0) '(1 . 0) 1 'curve-factor 0.75))
