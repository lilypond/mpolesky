;;; scheme2ps.scm

;;; miscellaneous

(define (solve-quadratic a b c)
  "Return pair with both solutions to Ax^2+Bx+C=0."
  (let ((d (- (* b b) (* 4 a c))))
    (cons (/ (+ (- b) (sqrt d)) (* 2 a))
          (/ (- (- b) (sqrt d)) (* 2 a)))))

(define (string-append-map f . lists)
  "Equivalent to (apply string-append (map f list0 list1 ...))."
  (apply string-append (apply map f lists)))

(define (get-opt-arg arg-in-list default)
  "Return arg from ARG-IN-LIST, or DEFAULT if ARG-IN-LIST is null."
  (if (null? arg-in-list) default (car arg-in-list)))

;; rounding, precision

(define (get-precision n . max-decimals)
  "Return fewest decimal-places (not more than MAX-DECIMALS -- 4 if
   unset) needed to represent N if N is a number, otherwise return 0."
  "Return lesser of MAX-DECIMALS (4 if unset) and fewest decimal-places
   needed to represent N if N is a number (0 otherwise)."
  ;; eg. (get-precision 123.1200) ==> 2
  ;; eg. (get-precision 123.123456) ==> 4
  (if (number? n)
      (let* ((max-decimals (get-opt-arg max-decimals 4))
             (x (round (* (real-part n) (expt 10 max-decimals)))))
        (let loop ((i max-decimals))
          (if (integer? (/ x (expt 10 i)))
              (- max-decimals i)
              (loop (- i 1)))))
      0))

(define (set-precision n . max-decimals)
  "Return N rounded to fewest decimal places needed (not more than
   MAX-DECIMALS -- 4 if unset) if N is a number, otherwise return N."
  ;; eg. (map (lambda (n) (set-precision n 4)) `(test 1/2 1.0 1.5 ,pi))
  ;;     ==> (test 0.5 1 1.5 3.1416)
  (if (number? n)
      (if (integer? n)
          (inexact->exact n)
          (let* ((k (expt 10.0 (get-opt-arg max-decimals 4)))
                 (float (/ (round (* (real-part n) k))
                           k)))
            (if (< (abs (- float (round (real-part n))))
                   (/ 0.1 k))
                (inexact->exact (round float))
                float)))
      n))

(define (get-format-escape x)
  "If X is a real number, return ~<n>f escape-string for ly:format,
   where n is precision of X. Otherwise return ~a escape-string."
  (if (real? x)
      (string-append "~" (number->string (get-precision x)) "f")
      "~a"))

(define (diff<1E-n? a b n)
  "Is the difference between A and B less than 10^-N?"
  (< (magnitude (- a b)) (expt 10 (- n))))

(define (=~ a b)
  ;; TODO: defend rationale for selecting 10^-13 as epsilon.
  "Is the difference between A and B less than 10^-13?
   ≅ is the symbol for almost-equal-to."
   ;; (=~ pi 3.1415926535897) ==> #t
   ;; (=~ pi 3.141592653589)  ==> #f
   ;; (=  0 (sin pi)) ==> #f
   ;; (=~ 0 (sin pi)) ==> #t
  (diff<1E-n? a b 13))

(define (round-to-float x . n)
  "Return X rounded to N decimal places (3 if unset) if
  (=~ rounded-X X) returns #t, otherwise return X."
  ;; (round-to-float 3.12299999999) ==> 3.123
  ;; (round-to-float 3.1230000001)  ==> 3.123
  ;; (round-to-float 3.123000001)   ==> 3.123000001
  ;; If the degree-based trig-functions defined below don't use this,
  ;; then: (= 0.5 (cos-deg (acos-deg 0.5))) ==> #f
  ;; However, I guess the following would be more appropriate anyway?
  ;;   (=~ 0.5 (cos-deg (acos-deg 0.5)))
  (let ((rounded-x (set-precision x (get-opt-arg n 3))))
    (if (=~ rounded-x x) rounded-x x)))
#!
(define (round-to-float x . n)
  ;; default is 14 because
  ;; (round-to-float (tan-deg 315) 15) ==> -0.999999999999999
  (let ((n (get-opt-arg n 14)))
    (/ (round (* x (expt 10 n))) (expt 10 n))))
!#
;;; pairs

(define (cons-map f x)
  "map F to contents of X"
  (cons (f (car x)) (f (cdr x))))

(define (map-on-pairs f . pairs)
  "map F across PAIRS as if they were 2-item lists."
  (cons (apply f (map car pairs))
        (apply f (map cdr pairs))))

;; defines 14 functions:
;; pair+    pair-     pair*     pair/
;; pair=    pair<     pair>     pair<=    pair>=
;; pairgcd  pairlcm   pairmin   pairmax   pairaverage.
;; eg. (pair+ '(1 . 1) '(2 . 3) '(5 . 8)) ==> (8 . 12)
;;     (pair<= '(1 . 8) '(1 . 5) '(2 . 3)) ==> (#t . #f)
;;     (pairmin '(1 . 8) '(1 . 5) '(2 . 3)) ==> (1 . 3)
(defmacro define-pair-function (f)
  `(define-public (,(symbol-append 'pair f) . pairs)
     (apply map-on-pairs ,f pairs)))
(defmacro define-pair-functions (fns)
  `(begin
    ,@(map (lambda (f)
              `(define-pair-function ,f))
            fns)))
(define-pair-functions (+ - * / = < > <= >= gcd lcm min max average))

(define (pair=? . pairs)
 (let ((test (apply map-on-pairs = pairs)))
   (and (car test) (cdr test))))

(define (pair=~? . pairs)
 (let ((test (apply map-on-pairs =~ pairs)))
   (and (car test) (cdr test))))

(define (pair->list pair)
  (list (car pair) (cdr pair)))



;;; list manipulation
#!
(define (mixed->pairs mix)
  "Organize list of single objects and pairs (MIX) into a new list of
   pairs and return the new list."
   ;; TODO: allow odd number of objects?
  ;; eg. (mixed->pairs '((0 . 1) 2 3)) ==> ((0 . 1) (2 . 3))
  (let loop ((pairs '()) (mix mix))
    (cond ((null? mix) pairs)
          ((pair? (car mix)) (loop (append pairs `(,(car mix)))
                                   (cdr mix)))
          (else (loop (append pairs `((,(car mix) . ,(cadr mix))))
                      (cddr mix))))))
!#

(define (mixed->pairs mix . make-last-obj-list?)
  "Organize list of single objects and pairs (MIX) into a new list of
   pairs and return the new list. If object count is odd, the last
   object will appended to the end of the list of pairs."
   ;; TODO: allow odd number of objects?
  ;; eg. (mixed->pairs '((0 . 1) 2 3)) ==> ((0 . 1) (2 . 3))
  (let loop ((pairs '()) (mix mix))
    (cond
     ((null? mix) pairs)
     ((pair? (car mix))
      (loop (append pairs `(,(car mix)))
            (cdr mix)))
     ((pair? (cdr mix))
      (loop (append pairs `((,(car mix) . ,(cadr mix))))
            (cddr mix)))
     (else (append pairs (if (or (null? make-last-obj-list?)
                                 (not (car make-last-obj-list?)))
                             mix
                             (list mix)))))))


(define (mixed->singles mix)
  "Organize list of single objects and pairs (MIX) into a new list of
   single objects and return the new list."
  ;; eg. (mixed->singles '(0 (1 . 2) foo (3 . 4))) ==> (0 1 2 foo 4 5)
  (let loop ((singles '()) (mix mix))
    (cond ((null? mix) singles)
          ((pair? (car mix))
            (loop (append singles `(,(caar mix) ,(cdar mix)))
                          (cdr mix)))
          (else (loop (append singles `(,(car mix))) (cdr mix))))))

;;; trigonometry

(define pi
  (angle -1))

(define (reduce-angle-deg angl . max-180?)
  "Return ANGL mapped to the interval [0,360) or to (-180,180] if
   max-180? is set."
  (let ((A (- angl (* 360 (floor (real-part (/ angl 360)))))))
    (cond ((or (null? max-180?) (not (car max-180?))) A)
          ((< 180 A) (- A 360))
          (else A))))

(define (deg->rad angl)
  (* angl pi 1/180))

(define (rad->deg angl)
  (let* ((inexact (/ angl pi 1/180))
         (integer (inexact->exact (round inexact))))
    (if (= angl (deg->rad integer)) integer inexact)))

(define (reduce-sin-angle-deg angl)
  (let ((A (reduce-angle-deg angl)))
    (cond ((<= 0 A 90) A)
          ((<= 90 A 270) (- 180 A))
          (else (- A 360)))))

(define (reduce-cos-angle-deg angl)
  (let ((A (reduce-angle-deg angl)))
    (if (<= 0 A 180) A (- 360 A))))

(define (reduce-tan-angle-deg angl)
  (let ((A (reduce-angle-deg angl)))
    (cond ((<= 0 A 90) A)
          ((<= 90 A 270) (- A 180))
          (else (- A 360)))))

(define (sin-deg x)
  (case (reduce-sin-angle-deg x)
    ((-90 -90.0) -1)
    ((  0   0.0)  0)
    (( 90  90.0)  1)
    (else (sin (deg->rad (reduce-sin-angle-deg x))))))

(define (cos-deg x)
  (case (reduce-cos-angle-deg x)
    ((  0   0.0)  1)
    (( 90  90.0)  0)
    ((180 180.0) -1)
    (else (cos (deg->rad (reduce-cos-angle-deg x))))))


(define (tan-deg x)
  (case (reduce-tan-angle-deg x)
    ((-45 -45.0) -1)
    ((  0   0.0)  0)
    (( 45  45.0)  1)
    (( 90  90.0 -90 -90.0) +nan.0)
    (else (tan (deg->rad (reduce-tan-angle-deg x))))))

(define (asin-deg x)
  (let* ((inexact (rad->deg (asin x)))
         (integer (inexact->exact (round inexact))))
    (if (= x (sin-deg integer)) integer inexact)))

(define (acos-deg x)
  (let* ((inexact (rad->deg (acos x)))
         (integer (inexact->exact (round inexact))))
    (if (= x (cos-deg integer)) integer inexact)))

(define (atan-deg x)
  (if (nan? x)
      +nan.0
      (let* ((inexact (rad->deg (atan x)))
             (integer (inexact->exact (round inexact))))
        (if (= x (tan-deg integer)) integer inexact))))


(define (angle-deg<? . angles)
  ;; TODO: allow complex-number angles?
  ;; Test if ANGLES are in a non-overlapping counter-clockwise order.
  ;; Return #f if there are consecutive duplicates.
  ;; (angle-deg<? 0 10 350) ==> #t
  ;; (angle-deg<? 10 350 0) ==> #t
  ;; (angle-deg<? 350 0 10) ==> #t
  ;; (angle-deg<? 350 10 0) ==> #f
  (let* ((angles (map reduce-angle-deg angles)) ;; [0,360)
         (A0 (car angles))
         (An (car (last-pair angles))))
    (apply < (if (< An A0)        ; (345 355 5 15) ==> (-15 -5 5 15)
                 (map (lambda (a) (if (<= A0 a) (- a 360) a)) angles)
                 angles))))

(define (angle-deg<=? . angles)
  ;; TODO: allow complex-number angles?
  ;; Test if ANGLES are in a non-overlapping counter-clockwise order.
  ;; Consecutive duplicates are allowed.
  ;; (angle-deg_< 0 10 350) ==> #t
  ;; (angle-deg_< 10 350 0) ==> #t
  ;; (angle-deg_< 350 0 10) ==> #t
  ;; (angle-deg_< 350 10 0) ==> #f
  (let* ((angles (map reduce-angle-deg angles)) ;; [0,360)
         (A0 (car angles))
         (An (car (last-pair angles))))
    (apply <= (if (< An A0)        ; (345 355 5 15) ==> (-15 -5 5 15)
                  (map (lambda (a) (if (<= A0 a) (- a 360) a)) angles)
                  angles))))


(define (angle-deg=? . angles)
  ;; Test if ANGLES are equal.
  ;; (angle-deg=? -10.1 349.9) ==> #t
  (apply = (map reduce-angle-deg angles)))

(define (angle-deg=~? . angles)
  ;; Test if ANGLES are almost equal.
  ;; (angle-deg=~? 0 1e-14) ==> #t
  (apply =~ (map reduce-angle-deg angles)))

;;; geometry

(define (a-b-dist a b)
  "Return the distance between two coordinate pairs."
  ;; eg. (a-b-dist '(0 . 0) '(1 . 1)) ==> 1.4142135623731
  (sqrt (+ (expt (- (car b) (car a)) 2)
           (expt (- (cdr b) (cdr a)) 2))))

(define (a-b-angle-deg a b)
  "Return the angle formed between line-segment A-B and the
   ray leaving point A to the right."
  ;; eg. (a-b-angle-deg '(0 . 0) '(1 . 1)) ==> 45
  ;; output range is [0,360)
  ;; functions used: (atan-deg)
  ;;                 (reduce-angle-deg)
  ;; used by: (a-points-to-b?)
  (let ((dx (- (car b) (car a)))
        (dy (- (cdr b) (cdr a))))
   (if (zero? dx)
       (cond ((zero?     dy)  0)
             ((positive? dy) 90)
             (else          270))
       (reduce-angle-deg (+ (if (positive? dx) 0 180)
                            (atan-deg (/ dy dx)))))))

#!
(define (get-angle-deg . points)
  "For 1 point, return vector-angle from origin; for 2, vector-angle
   formed; for 3, *acute* angle formed."
!#

(define (a-b-slope a b)
  (let ((dx (- (car b) (car a)))
        (dy (- (cdr b) (cdr a))))
   (if (zero? dx) +nan.0 (/ dy dx))))

(define (vector-offset angle-deg dist . P0)
  "Return the coordinate pair of the point reached when moving DIST
   units from P0 (or origin if unset) at ANGLE-DEG."
  ;; eg. (vector-offset 90 1) ==> (0 . 1)
 ; (cons-map
 ;   round-to-float
    (map-on-pairs + (get-opt-arg P0 '(0 . 0))
                    (cons (* dist (cos-deg angle-deg))
                          (* dist (sin-deg angle-deg)))));)

(define (a-points-to-b? a b angl)
  ;; eg. (a-points-to-b? '(0 . 0) '(1 . 1) 45) ==> #t
  ;; having an error margin seems necessary.
  ;; functions used: (a-b-angle-deg)
  ;;                 (reduce-angle-deg)
  ;; used by: (ray-intersection)
  (let ((angle-error-margin 1))
    (or (equal? a b)
        (< (abs (- (a-b-angle-deg a b)
                   (reduce-angle-deg angl)))
           angle-error-margin))))

(define (rotate-point angl point . center)
  "Return POINT rotated ANGL degrees around CENTER (or origin if
   unset)."
  ;; eg. (rotate-point 90 '(1 . 0)) ==> (0 . 1)
  (let ((center (get-opt-arg center '(0 . 0))))
    (vector-offset (+ angl (a-b-angle-deg center point))
                   (a-b-dist center point)
                   center)))

(define (rotate-points angl points . center)
  "Return list of POINTS rotated ANGL degrees around CENTER (origin if
   unset)."
  ;; eg. (rotate-points 90 '((1 . 0) (0 . 1)))
  ;;     ==> ((0 . 1) (-1 . 0))
  (let* ((points (mixed->pairs points))
         (center (get-opt-arg center '(0 . 0))))
    (map (lambda (p) (rotate-point angl p center))
         points)))

(define (line-intersection line1 line2) ; line is '(point . angle)
  ;; (line-intersection '((0 . 0) . 90) '((1 . 1) . 180))
  ;; functions used: (tan-deg)
  ;; used by: (ray-intersection)
  (let* ((P1 (car line1))       (angle1 (cdr line1))
         (P2 (car line2))       (angle2 (cdr line2))
         (x1 (car P1))          (y1 (cdr P1))
         (x2 (car P2))          (y2 (cdr P2))
         (m1 (tan-deg angle1))  (m2 (tan-deg angle2))
         (b1 (- y1 (* m1 x1)))  (b2 (- y2 (* m2 x2)))
         ;; vertical slopes are +nan.0 :
         (x (if (nan? m1)
                (if (nan? m2) +nan.0 x1)
                (cond ((nan? m2) x2)
                      ;; TODO: use =~ instead?
                      ((= m1 m2) +nan.0)
                      (else (/ (- b2 b1) (- m1 m2))))))
         (y (if (nan? m1)
                (+ b2 (* m2 x))
                (+ b1 (* m1 x)))))
    ;; or-map is in ice-9/boot-9
    (if (or-map nan? `(,x ,y))
        #f
        (cons-map round-to-float (cons x y)))))

(define (ray-intersection ray1 ray2) ; ray is '(point . angle)
  ;; (ray-intersection '((0 . 0) . 90) '((1 . 1) . 180)) ==> (0 . 1)
  ;; functions used: (line-intersection)
  ;;                 (a-points-to-b?)
  (let ((P+ (line-intersection ray1 ray2))
        (P1 (car ray1))  (angle1 (cdr ray1))
        (P2 (car ray2))  (angle2 (cdr ray2)))
    (if (and P+
             (a-points-to-b? P1 P+ angle1)
             (a-points-to-b? P2 P+ angle2))
        P+
        #f)))

(define (3points->circle P0 P1 P2)
  ;; (3points->circle '(0 . 0) '(0 . 1) '(1 . 0))
  ;;   ==> ((0.5 . 0.5) . 0.707106781186548)
  ;; gives (center . radius)
  ;; Return #f if any 2 points are the same or if no such circle exists.
  (if (or (pair=? P0 P1) (pair=? P0 P2) (pair=? P1 P2))
      #f
      (let* ((M0 (map-on-pairs average P0 P1)) ;(pairaverage P0 P1))
             (M1 (map-on-pairs average P1 P2))
             (angle0 (a-b-angle-deg P0 P1))
             (angle1 (a-b-angle-deg P1 P2))
             (center (line-intersection (cons M0 (+ angle0 90))
                                        (cons M1 (+ angle1 90))))
             (radius (if center (a-b-dist center P0) #f)))
        (cons center radius))))

(define (bezier-midpoint P0 P1 P2 P3)
  (map-on-pairs average P0 P1 P1 P1 P2 P2 P2 P3))

(define (x-mirror p w)
  "Return the point horizontally opposite point P within width W.
  (x-mirror '(1 . 2) 10) ==> '(9 . 2)"
  (cons (- w (car p)) (cdr p)))

(define (y-mirror p h)
  "Return the point vertically opposite point P within height H."
  (cons (car p) (- h (cdr p))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; postscript-code generators

(define default-curve-factor
  ;; "Magic number that makes beziers approximate ellipses."
  ; (I think)
  ;; see "y1" on p.3 of http://www.tinaja.com/glib/bezcirc.pdf
  ;; 0.552284749830793
  (* 4/3 (- (sqrt 2) 1)))

(define dcf default-curve-factor)

(define (objects->ps-string . objects)
  ;; accepts any number of items (numbers, symbols, strings, and pairs)
  ;; eg. (objects->ps-string 0 `(,(sqrt 2) . 2) 'dup pi)
  ;;     ==> "0 1.4142 2 dup 3.1416 "
  ;; functions used: (format4)
  ;;                 (mixed->singles)
  ;; used by: all operators listed in unchained-stack-operators
  (string-append-map
   (lambda (x) (string-append (ly:format (get-format-escape x) x) " "))
   (mixed->singles objects)))
#!

;; these are defined individually below.
(define chained-stack-operators
  '(arct
    arcto
    lineto
    rlineto))
!#

(defmacro define-nullary-operators (operators)
  `(begin
    ,@(map (lambda (operator)
             `(define-public (,operator)
               ,(string-append (symbol->string operator) "\n")))
            operators)))


(defmacro define-unchained-stack-operators (operators)
  `(begin
    ,@(map (lambda (operator)
             `(define-public (,operator . objects)
               (string-append
                 (apply objects->ps-string objects)
                  ,(string-append (symbol->string operator) "\n"))))
            operators)))

(define-nullary-operators
  (closepath
   fill
   gsave
   grestore
   newpath
   stroke))

(define-unchained-stack-operators
  (arc
   arcn
   moveto
   curveto
   rcurveto
   rmoveto
   rectfill
   rectstroke
   rotate
   scale
   translate))

(define (arct radius . points)
  "Return a chain of postscript arct commands for the given POINTS
   and RADIUS. Note that RADIUS comes before POINTS, which is the
   reverse of the normal order in postscript. Example:
   (arct 0.1 '(0 . 1) '(2 . 3) '(4 . 5))
   ==> 0 1 2 3 0.1 arct\n2 3 4 5 0.1 arct\n"
  (let loop ((str "") (rest (mixed->pairs points)))
    (if (< (length rest) 2)
        str
        (loop (string-append
                str
                (objects->ps-string (car rest) (cadr rest) radius)
                "arct\n")
              (cdr rest)))))

(define (arcto radius . points)
  "Return a chain of postscript arcto commands for the given POINTS
   and RADIUS. Note that RADIUS comes before POINTS, which is the
   reverse of the normal order in postscript. Example:
   (arcto 0.1 '(0 . 1) '(2 . 3) '(4 . 5))
   ==> 0 1 2 3 0.1 arcto\n2 3 4 5 0.1 arcto\n"
  (let loop ((str "") (rest (mixed->pairs points)))
    (if (< (length rest) 2)
        str
        (loop (string-append
                str
                (objects->ps-string (car rest) (cadr rest) radius)
                "arcto\n")
              (cdr rest)))))

(define (lineto . points)
  "Return a chain of postscript lineto commands for the given POINTS.
   Example: (lineto '(0 . 1) '(2 . 3)) ==> 0 1 lineto\n2 3 lineto\n"
  (string-append-map
    (lambda (p) (string-append (objects->ps-string p) "lineto\n"))
    (mixed->pairs points)))

(define (rlineto . points)
  "Return a chain of postscript rlineto commands for the given POINTS.
   Example: (rlineto '(0 . 1) '(2 . 3)) ==> 0 1 rlineto\n2 3 rlineto\n"
  (string-append-map
    (lambda (p) (string-append (objects->ps-string p) "rlineto\n"))
    (mixed->pairs points)))

(define (bind . str)
  ;; TODO: this may need some refining to prevent whitespace errors.
  (string-append "{"
                 ;; all strings *should* end with a space or newline:
                 (string-drop-right (apply string-append str) 1)
                 "} bind "))

(define (repeat i . str)
  ;; TODO: this may need some refining to prevent whitespace errors.
  (string-append
   (number->string i)
   (if (char=? #\{ (string-ref (car str) 0))
       " "
       " {")
   ;; unbracketed strings *should* end with space or newline:
   (if (char=? #\{ (string-ref (car str) 0))
       (apply string-append str)
       (string-drop-right (apply string-append str) 1))
   (if (char=? #\{ (string-ref (car str) 0))
       "repeat\n"
       "} repeat\n")))


;;; special curveto procedures

;; TODO: allow these to take points as single numbers?

#!

                vertex
  generic     .
 /   |   \  .
Z    U    V
         / \
        L   arch



!#


(define (generic-curveto style P0 P3 A0 A1 cf0 cf1 . h)
  (let*
    ((X0 (case style
          ((Z) (line-intersection (cons P0 A0) (cons P3 (+ A0 90))))
          ((V) (ray-intersection  (cons P0 A0) (cons P3 (+ A1 180))))
          ((U) (vector-offset A0 (* 4/3 (car h)) P0))))

     (X1 (case style
          ((Z) (map-on-pairs - P3 (map-on-pairs - X0 P0)))
          ((V) X0)
          ((U) (map-on-pairs + (map-on-pairs - X0 P0) P3))))
         ;; be careful with signs...
     (P1 (vector-offset A0
                        (* cf0 (a-b-dist P0 X0))
                        P0))
     (P2 (vector-offset (+ A1 180)
                        (* cf1 (a-b-dist P3 X1))
                        P3)))
   (curveto P1 P2 P3)))

(define (generic-rcurveto style P0 P3 A0 A1 cf0 cf1 . h)
  (let*
    ((X0 (case style
          ((Z) (line-intersection (cons P0 A0) (cons P3 (+ A0 90))))
          ((V) (ray-intersection  (cons P0 A0) (cons P3 (+ A1 180))))
          ((U) (vector-offset A0 (* 4/3 (car h)) P0))))

     (X1 (case style
          ((Z) (map-on-pairs - P3 (map-on-pairs - X0 P0)))
          ((V) X0)
          ((U) (map-on-pairs + (map-on-pairs - X0 P0) P3))))
         ;; be careful with signs...
     (P1 (vector-offset A0
                        (* cf0 (a-b-dist P0 X0))
                        P0))
     (P2 (vector-offset (+ A1 180)
                        (* cf1 (a-b-dist P3 X1))
                        P3)))
   (rcurveto P1 P2 P3)))

(define (generic-warning problem function given fallback)
  (apply ly:warning
    (string-append
     "invalid " problem " in " function ": "
     (if (number? given)
         (ly:format (get-format-escape given) given)
         "~a")
     (if (string? fallback)
         (string-append " (using ~a instead)")
         (string-append
           " (changed to "
           (ly:format (get-format-escape fallback) fallback)
           ")")))
    (if (not (number? given))
        (if (not (number? fallback))
            (list given fallback)
            (list given))
        '())))

(define (generic-vertex-curveto relative? P0 args)
  "Return postscript code for an arch-shaped bezier curve.
   ARGS are: V P3 [crv-fctrs]. P3 is terminal point, V
   (vertex) is the intersection of lines P0-P1 and P2-P3. CF0 is a
   curve-factor that determines where P1 is placed along segment P0-V,
   and CF1 similarly determines where P2 is placed along segment P3-V.
   If neither curve-factor is given, both are set to default-curve-
   factor (which approximates an ellipse quarter-arc). If only CF0 is
   given, CF1 = CF0."
  ;; TODO: disallow cfs not in [0,1]? Leaning towards no.
  ;; (generic-vertex-curveto  #f
  ;;                          '(0 . 1)
  ;;                          '((1 . 1) (1 . 0) (2/3 . 3/4)))
  ;;   ==> "0.6667 1 1 0.75 1 0 curveto\n"
  (let* ((V  (list-ref args 0))
         (P3 (list-ref args 1))
         (crv-fctrs (list-tail args 2))
         (cfs (if (null? crv-fctrs) (cons dcf dcf) (car crv-fctrs)))
         (cf0 (if (null? (car cfs)) dcf (car cfs)))
         (cf1 (if (null? (cdr cfs)) cf0 (cdr cfs))))
    (cond
     ((pair=? P0 P3) "")
     ((or (pair=? P0 V)
          (pair=? P3 V)
          (angle-deg=? (a-b-angle-deg P0 V)
                       (a-b-angle-deg P0 P3)))
        (if relative?
            (begin
              (generic-warning "vertex" "vertex-rcurveto" V "rlineto")
              (rlineto P3))
            (begin
              (generic-warning "vertex" "vertex-curveto" V "lineto")
              (lineto P3))))
     (else
       (let ((P1 (vector-offset (a-b-angle-deg P0 V)
                                (* cf0 (a-b-dist P0 V))
                                P0))
             (P2 (vector-offset (a-b-angle-deg P3 V)
                                (* cf1 (a-b-dist P3 V))
                                P3)))
         ((if relative? rcurveto curveto) P1 P2 P3))))))

(define (vertex-curveto . args)
  "Return postscript code for an arch-shaped bezier curve.
   ARGS are: P0 V P3 [cf0 [cf1]]. P0 and P3 are terminal points, V
   (vertex) is the intersection of lines P0-P1 and P2-P3. CF0 is a
   curve-factor that determines where P1 is placed along segment P0-V,
   and CF1 similarly determines where P2 is placed along segment P3-V.
   If neither curve-factor is given, both are set to default-curve-
   factor (which approximates an ellipse quarter-arc). If only CF0 is
   given, CF1 = CF0."
   (let* ((pairs (mixed->pairs args #t))
          (P0 (car pairs)))
     (generic-vertex-curveto #f P0 (cdr pairs))))

(define (vertex-rcurveto . args)
  "Return postscript code for an arch-shaped bezier curve.
   ARGS are: rel-V rel-P3 [cf0 [cf1]]. REL-P3 is the relative offset
   from P0 to P3, REL-V (vertex) is the relative offset from P0 to the
   intersection of lines P0-P1 and P2-P3. CF0 is a curve-factor that
   determines where P1 is placed along segment P0-V, and CF1 similarly
   determines where P2 is placed along segment P3-V. If neither curve-
   factor is given, both are set to default-curve-factor (which
   approximates an ellipse quarter-arc). If only CF0 is given,
   CF1 = CF0."
  (generic-vertex-curveto #t '(0 . 0) (mixed->pairs args #t)))


(define (vertex-curveto-midpoint . args)
  (let* ((pairs (mixed->pairs args #t))
         (P0 (list-ref pairs 0))
         (V  (list-ref pairs 1))
         (P3 (list-ref pairs 2))
         (crv-fctrs (list-tail pairs 3))
         (cfs (if (null? crv-fctrs) (cons dcf dcf) (car crv-fctrs)))
         (cf0 (if (null? (car cfs)) dcf (car cfs)))
         (cf1 (if (null? (cdr cfs)) cf0 (cdr cfs)))
         (P1 (vector-offset (a-b-angle-deg P0 V)
                            (* cf0 (a-b-dist P0 V))
                            P0))
         (P2 (vector-offset (a-b-angle-deg P3 V)
                            (* cf1 (a-b-dist P3 V))
                            P3)))
    (bezier-midpoint P0 P1 P2 P3)))


(define (Z-curveto P0 P3 A0 crv-fctr0 crv-fctr1)
  ;; I think we could define this as:
  ;; bezier-curve where ray P0->P1 does not intersect with ray P3->P2
  ;; No. not quite. How about
  ;;
  ;; TODO: (curveto-by-lengths P0 P3 A0 A1 length0 length1)
  ;; TODO: (curveto-by-vertex P0 vertex P3 cf0 cf1)
  ;; TODO: (rcurveto-by-lengths P3 A0 A1 length0 length1)
  ;;
  ;; ; (curveto-by-factors P0 P3 A0 A1 cf0 cf1) ; this is vague.
  ;; ; (curveto P0 P3 A0 A1 .
  ;;           curve-factor_or_segment-length? val0 val1)
  ;; ; (rcurveto-by-factors P3 A0 A1 cf0 cf1)
  ;;
  ;;  __
  ;;    \__  bezier-curve with A0 = A1
  ;;
  ;;  /__
  ;;     \
  ;;
  ;;   /\___   <- I think this is not one.
  ;;  /
  ;;
  ;;     /
  ;;   \|
  ;;
  ;;
  ;; (Z-curveto '(0 . 1) '(1 . 0) 0 0.75 0.75)
  ;;    ==>  "0.75 1 0.25 0 1 0 curveto\n"
  ;; will degrade to lineto if it can't be done.
  (cond ((pair=? P0 P3) "")
        ((a-points-to-b? P0 P3 A0) (lineto P3))
        ((let ((An (a-b-angle-deg P0 P3)))
          (not (angle-deg<=? (- An 90) A0 (+ An 90)))) (lineto P3))
        (else (generic-curveto 'Z P0 P3 A0 A0 crv-fctr0 crv-fctr1))))

(define (U-curveto P0 P3 h)
  ;; TODO: (U-rcurveto P3 h)
  ;;    _
  ;;   | |   symmetrical bezier-curve with A1 = A0 + 180
  ;;
  ;; (U-curveto '(0 . 0) '(1 . 0) 1)
  ;;   ==> "0 1.333 1 1.333 1 0 curveto\n"
  ;; degrades to lineto if h=0.
  (cond ((pair=? P0 P3) "")
        ((zero? h) (lineto P3))
        (else (let ((A0 ((if (positive? h) + -)
                         (a-b-angle-deg P0 P3) 90)))
                (generic-curveto 'U P0 P3 A0 (+ A0 180) 1 1 h)))))

(define (U-rcurveto P3 h)
  ;; TODO: make this better.
  ;; (U-rcurveto '(1 . 0) 1)
  ;;   ==> "0 1.333 1 1.333 1 0 curveto\n"
  (cond ((pair=? '(0 . 0) P3) "")
        ((zero? h) (lineto P3))
        (else (let ((A0 ((if (positive? h) + -)
                         (a-b-angle-deg '(0 . 0) P3) 90)))
                (generic-rcurveto 'U '(0 . 0) P3 A0 (+ A0 180) 1 1 h)))))


(define (arch-curveto-angle P0 P3 h angl)
  ;; TODO: (arch-rcurveto-angle P3 h angl)
  ;; subset of V-curveto
  ;;    _
  ;;   / \   symmetrical bezier-curve
  ;;
  ;; (arch-curveto-angle '(0 . 0) '(1 . 0) 1 78.299)
  ;;   ==> "0.2761 1.3333 0.7239 1.3333 1 0 curveto\n"
  ;; If necessary, ANGL will be changed to the closest angle that
  ;; creates a proper arch.
  ;; direction of ANGL overrides sign of H.
  (let* ((h          (abs h))
         (A03        (a-b-angle-deg P0 P3))
         (A03-90     (reduce-angle-deg (- A03 90)))
         (A03+90     (reduce-angle-deg (+ A03 90)))
         (P0P3-mid   (map-on-pairs average P0 P3))
         (apex       (map-on-pairs + P0P3-mid (vector-offset A03+90 h)))
         (min-vertex-L (map-on-pairs + P0P3-mid (vector-offset A03+90 (* h 4/3))))
         (min-vertex-R (map-on-pairs - P0P3-mid (vector-offset A03+90 (* h 4/3))))
         (min-angle-L  (a-b-angle-deg P0 min-vertex-L))
         (min-angle-R  (a-b-angle-deg P0 min-vertex-R))
         (A0 (reduce-angle-deg
              (cond ((angle-deg<? A03 angl min-angle-L) min-angle-L)
                    ((angle-deg<=? A03+90 angl (+ A03 180)) A03+90)
                    ((angle-deg<? (+ A03 180) angl A03-90)  A03-90)
                    ((angle-deg<? min-angle-R angl A03) min-angle-R)
                    (else angl))
              (if (negative? angl) #t #f))))
     ;; TODO: use when (non-standard guile) instead?
     (if (not (or (angle-deg<=? A03-90 angl min-angle-R)
                  (angle-deg<=? min-angle-L angl A03+90)))
         (generic-warning "angle" "arch-curveto" angl A0)
      )

  (cond ((pair=? P0 P3) "")
        ;; TODO: use =~ instead?
        ((or (zero? h) (angle-deg=? A03 A0)) (lineto P3))
        ((= 90  (reduce-angle-deg (- A0 A03))) (U-curveto P0 P3 h))
        ((= 270 (reduce-angle-deg (- A0 A03))) (U-curveto P0 P3 (- h)))
        (else
         (let* ((A1 (reduce-angle-deg (- (* A03 2) A0)))
                (X0 (line-intersection (cons P0 A0) (cons P3 A1))))

          ;; TODO: remove redundant code:
          (if X0
              (if (not (angle-deg<=? A03-90 A0 A03+90))
                  (U-curveto P0 P3 h)
                  (let* ((base-vertex-dist (a-b-dist X0 P0P3-mid))
                         (crv-fctr (/ h 3/4 base-vertex-dist)))
                    (if (< 1 crv-fctr)
                        (lineto apex P3)
                        (generic-curveto 'V P0 P3 A0 A1
                                         crv-fctr crv-fctr))))
              (lineto apex P3)))))))

(define (arch-curveto-factor P0 P3 h crv-fctr)
  ;; TODO: (arch-rcurveto-factor P3 h crv-fctr)
  ;; subset of V-curveto
  ;;    _
  ;;   / \   symmetrical bezier-curve
  ;;
  ;; (arch-curveto-factor '(0 . 0) '(1 . 0) 1 dcf)
  ;;   ==> "0.2761 1.3333 0.7239 1.3333 1 0 curveto\n"
  ;; If necessary, CRV-FCTR will be changed to the closest curve-factor
  ;; that creates a proper arch.
  (let ((new-crv-fctr (cond ((<= crv-fctr 0) 0)
                            ((<= 1 crv-fctr) 1)
                            (else crv-fctr))))
     ;; TODO: use "when" (non-standard guile) instead?
     (if (and (not (<= 0 crv-fctr 1))
              (not (zero? h)))
      ;; (and-map not `(,(<= 0 crv-fctr 1) ,(zero? h)))
         (curveto-warning
           "arch-curveto" "curve-factor" crv-fctr new-crv-fctr))
  (cond ((pair=? P0 P3) "")
        ((zero? h) (lineto P3))
        ((zero? new-crv-fctr) (U-curveto P0 P3 h))
        (else
         (let* ((base-vertex-dist (/ h 3/4 new-crv-fctr))
                (A03 (a-b-angle-deg P0 P3))
                (vertex (map-on-pairs + (map-on-pairs average P0 P3)
                               (vector-offset (+ A03 90)
                                              base-vertex-dist)))
                (A0 (a-b-angle-deg P0 vertex))
                (A1 (a-b-angle-deg vertex P3)))
            (generic-curveto 'V P0 P3 A0 A1 new-crv-fctr new-crv-fctr))))))

(define (arch-curveto P0 P3 h . opt-args)
  ;; TODO: (arch-rcurveto P3 h . opt-args)
  ;; subset of V-curveto
  ;; opt-args is: option value.
  ;; option= 'angle or 'curve-factor
  ;; defaults to: 'curve-factor default-curve-factor.
  ;;    _
  ;;   / \   symmetrical bezier-curve
  ;;
  ;; (arch-curveto '(0 . 0) '(1 . 0) 1)
  ;;   ==> "0.2761 1.3333 0.7239 1.3333 1 0 curveto\n"
  ;; (arch-curveto '(0 . 0) '(1 . 0) 1 'angle 69.444)
  ;;   ==> "0.5 1.3333 0.5 1.3333 1 0 curveto\n"
  ;; (arch-curveto '(0 . 0) '(1 . 0) 1 'curve-factor 0.75)
  ;;   ==> "0.375 1.3333 0.625 1.3333 1 0 curveto\n"
  ;; If necessary, OPT-ARG will be changed to the closest value
  ;; that creates a proper arch.
  (cond ((pair=? P0 P3) "")
        ((zero? h) (lineto P3))
        (else
         (if (null? opt-args)
             (arch-curveto-factor P0 P3 h dcf)
             ((case (car opt-args)
                 ((angle) arch-curveto-angle)
                 ((curve-factor) arch-curveto-factor))
                P0 P3 h (cadr opt-args))))))

(define (L-curveto . args)
  ;; TODO: (L-rcurveto . args) ; args = P3 A0 [cf0 [cf1]]
  "Return postscript code for a right-angle bezier curve.
   ARGS are: P0 P3 A0 [cf0 [cf1]]. P0 and P3 are terminal points, A0 is
   initial angle, and CF0 / CF1 are optional curve-factors in the range
   [0,1] such that if both are 0, a line is drawn, and if both are 1,
   the curve is drawn with maximum curvature. If neither is given, both
   are set to default-curve-factor (which approximates an ellipse
   quarter-arc). If only CF0 is given, CF1 = CF0. A0 must be within 90
   degrees of the vector angle P0->P3."
  ;; subset of V-curveto
  ;;
  ;;   |_    bezier-curve with A1 = A0 +/- 90
  ;;
  ;; (L-curveto '(0 . 1) '(1 . 0) -90 0.75 0.75)
  ;;   ==> "0 0.25 0.25 0 1 0 curveto\n"
  (let* ((args (mixed->singles args))
         (P0 (cons (list-ref args 0) (list-ref args 1)))
         (P3 (cons (list-ref args 2) (list-ref args 3)))
         (A0 (list-ref args 4))
         (crv-fctrs (list-tail args 5))
         (crv-fctrs (if (null? crv-fctrs) `(,dcf ,dcf) crv-fctrs))
         (cf0 (car crv-fctrs))
         (cf1 (if (null? (cdr crv-fctrs)) cf0 (cadr crv-fctrs)))
         (new-cf0 (cond ((<= cf0 0) 0) ((<= 1 cf0) 1) (else cf0)))
         (new-cf1 (cond ((<= cf1 0) 0) ((<= 1 cf1) 1) (else cf1)))
         (A03 (a-b-angle-deg P0 P3)))
    (cond ((pair=? P0 P3) "")
        ((or (angle-deg=? A0 A03)
             (<= 90 (reduce-angle-deg (abs (- A0 A03)))))
          (begin (generic-warning "angle" "L-rcurveto" A0 "lineto")
                 (lineto P3)))
        (else
         (if (not (<= 0 cf0 1))
             (generic-warning "curve-factor" "L-curveto" cf0 new-cf0))
         (if (not (<= 0 cf1 1))
             (generic-warning "curve-factor" "L-curveto" cf1 new-cf1))
         (let* ((vertex (line-intersection (cons P0 A0)
                                           (cons P3 (+ A0 90))))
                (A1 (a-b-angle-deg vertex P3)))
           (if (not (a-points-to-b? P0 vertex A0))
               (lineto P3)
               (V-curveto P0 P3 A0 A1 new-cf0 new-cf1)))))))



(define (V-curveto P0 P3 A0 A1 crv-fctr0 crv-fctr1)
  ;; TODO: (V-rcurveto P3 A0 A1 crv-fctr0 crv-fctr1)
  ;;
  ;;   __/   bezier-curve with A0 != A1
  ;;
  ;; note that there must be a point X0 such that:
  ;;   a) A0 points directly towards X0, and
  ;;   b) A1 points directly away from X0.
  ;;
  ;; (V-curveto '(0 . 0) '(1 . 1) 0 60 0.75 0.75)
  ;;   ==> "0.317 0 0.567 0.25 1 1 curveto\n"
  ;; will degrade to lineto if it can't be done.
  (cond ((pair=? P0 P3) "")
        ((a-points-to-b? P0 P3 A0) (lineto P3))
        ((a-points-to-b? P0 P3 A1) (lineto P3))
        (else
          (let* ((X0 (line-intersection (cons P0 A0) (cons P3 A1))))
            (if (not (and (a-points-to-b? P0 X0 A0)
                          (a-points-to-b? X0 P3 A1)))
              (lineto P3)
              (generic-curveto 'V P0 P3 A0 A1 crv-fctr0 crv-fctr1))))))

#!

  ;; (A03 (a-b-angle-deg P0 P3))
  ;; direction:  angl points L (positive) or R (negative) or #f
  ;;             when facing from P0 to P3?
  ;;             (let* ((dir (reduce-angle-deg (- angl A03)))
  ;;                    (direction (if (< dir 180) dir (- dir 360)))
  ;; deflection: angl within 90 deg of A03 (in either direction)?
  ;; MID03: (pairaverage P0 P3)
(define (V-curveto P0 P3 A0 A1 crv-fctr0 crv-fctr1)
  (let* ((A03 (a-b-angle-deg P0 P3))
         (A03-90 (reduce-angle-deg (- A03 90)))
         (A03+90 (reduce-angle-deg (+ A03 90)))

         (A0-range (cons (if (< A03+90 A03-90) (- A03-90 360) A03-90)
                         A03+90))
         (A1-range (cons (if (< A03-90 A03+90) (- A03+90 360) A03+90)
                         A03-90))
         (divergence0 (reduce-angle-deg (- A0 A03) #t))
         (divergence1 (reduce-angle-deg (- A03 A1) #t))
         (A0 (cond ((< divergence0 -90)
                    (begin
                     (ly:warning (_ "V-curveto: A0 too wide."))
                     (reduce-angle-deg (- A03 90))))
                   ((> divergence0 90)
                    (begin
                     (ly:warning (_ "V-curveto: A0 too wide."))
                     (reduce-angle-deg (+ A03 90)))))))
   (list A0-range A1-range)))

         !#

;;; convex hulls and bounding-boxes

(define (rect-hull w h)
  ;; TODO: should these be pairs?
  (list 0 0 0 h w h w 0))

(define (exts->dims extents)
  (cons (- (cdar extents) (caar extents))
        (- (cddr extents) (cadr extents))))

(define (get-bbox-extents points)
 ;; eg. (get-bbox-extents '((4 . 0) (3 . 1) (5 . 2)))
 ;;     ==> ((3 . 5) 0 . 2) <<same as>> ((3 . 5) . (0 . 2))
 (let* ((points (mixed->pairs points))
        (mins (apply map-on-pairs min points))
        (maxs (apply map-on-pairs max points)))
   (cons (cons (car mins) (car maxs))
         (cons (cdr mins) (cdr maxs)))))

(define (get-bbox-dimensions points)
  (exts->dims (get-bbox-extents points)))


;; rotating and flipping pictograms

(define (rotate-image angl rotated-exts)
  (if (zero? angl)
      ""
      (string-append (translate (- (caar rotated-exts))
                                (- (cadr rotated-exts)))
                     (rotate angl))))

(define (flip-image flip w h)
  (case flip
    ((x) (string-append (translate w 0) (scale -1 1)))
    ((y) (string-append (translate 0 h) (scale 1 -1)))
    (else "")))

(define (rotate-and-flip-image angl flip rotated-exts)
  ;; confusingly, this approach requires postscript to flip *before*
  ;; rotating to create the effect of rotating first, then flipping.
  (let ((rotated-dims (exts->dims rotated-exts)))
    (string-append
      (flip-image flip (car rotated-dims) (cdr rotated-dims))
      (rotate-image angl rotated-exts))))

(define (get-rotated-exts orig-convex-hull angl . exceptions)
  "Given list of coordinate pairs ORIG-CONVEX-HULL and angle of rotation
   ANGL, return new X- and Y-extents (after rotating around the origin)
   as '((xmin . xmax) . (ymin . ymax)). Optional EXCEPTIONS are:
   [xmin xmax ymin ymax] where each arg is a list of lists, the
   innermost lists having the form (A0 A1 exception) which expands to
   (if (< A0 angl A1) exception). If exceptions are given, all 4 corners
   must be represented, even if some are '(), though logically if one
   exception is needed, so are the other three."
   #!
   (get-rotated-exts '((0 . 0) (1 . 1)) 90)
      ==> ((-1 . 0) 0 . 1)
   (get-rotated-exts '((0 . 0) (1 . 1)) 90 '((85 95 -2)) '() '() '())
      ==> ((-2 . 0) 0 . 1)
   !#
 (let*
   ((rotated-hull-exts
      (get-bbox-extents (rotate-points angl orig-convex-hull)))
   (this-extent
     (lambda (extract arglist)
     ;; extract: xmin=caar; xmax=cdar; ymin=cadr; ymax=cddr
     ;; arglist: ((A0 A1 exception0) (A2 A3 exception1) ...)
     (let loop ((this-arglist arglist))
       (if (null? this-arglist)
           (extract rotated-hull-exts)
           (let* ((A0        (reduce-angle-deg (caar this-arglist)))
                  (A1        (reduce-angle-deg (cadar this-arglist)))
                  (exception (caddar this-arglist)))
             (if (angle-deg<=? A0 angl A1)
                 exception
                 (loop (cdr this-arglist)))))))))
     (if (null? exceptions)
         rotated-hull-exts
         (cons (cons (this-extent caar (car exceptions))
                     (this-extent cdar (cadr exceptions)))
               (cons (this-extent cadr (caddr exceptions))
                     (this-extent cddr (cadddr exceptions)))))))



(define (get-rotated-circle-bboxes angl circles)
  "Given angle of rotation ANGL and list CIRCLES, where each circle has
   the form (cons center radius) -- CENTER being a coordinate pair,
   return a list of coordinate pairs representing the lower-left and
   upper-right corners of each circle after rotating around the origin.
   ie. (LL0 UR0 LL1 UR1 ...)."
  (let loop ((rotated-bboxes '()) (remaining circles))
    (if (null? remaining)
        rotated-bboxes
        (let ((cntr (rotate-point angl (caar remaining)))
              (r    (cdar remaining)))
          (loop (append rotated-bboxes
                        (list (map-on-pairs - cntr (cons r r))
                              (map-on-pairs + cntr (cons r r))))
                (cdr remaining))))))
